package aaa.pfe.auth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

/** This class manages the creation of configuration setups
 * for the 3 authentication schemes
 * Author: Zauwali S. Paki
 * April 2018
 * */
public class CreateSettings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_settings);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.create_setting_activity_title);

        String buttonIconName = "ic_settings_white";
        String[] textViewContent = new String[]{"Pin Code", "Pattern Lock", "Passfaces"};

        final CreateSettingsAdapter adapter = new CreateSettingsAdapter(
                CreateSettings.this, buttonIconName, textViewContent);
        RecyclerView recyclerView = findViewById(R.id.create_settings_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(CreateSettings.this, HomeUI.class);
        finish();
        startActivity(intent);
        return true;
    }
}
