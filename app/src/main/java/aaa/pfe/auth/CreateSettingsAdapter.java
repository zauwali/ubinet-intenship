package aaa.pfe.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import aaa.pfe.auth.view.passface.PassFaceAdminActivity;
import aaa.pfe.auth.view.pincode.PinCodeAdminActivity;
import aaa.pfe.auth.view.schemepattern.SchemeAdminActivity;
import de.hdodenhof.circleimageview.CircleImageView;

/** The class that handles the task of creation of setups for the
 * three authentication mechanism.
 * Created by Zauwali S. Paki
 * April 2018
 */

public class CreateSettingsAdapter extends RecyclerView.Adapter<CreateSettingsAdapter.ViewHolder> {

    private Context context;
    private String mainButtonIconName;
    private String[] textViewContent;
    CreateSettingsAdapter(Context context, String mainButtonIconName, String[] textViewContent){

        this.context = context;
        this.mainButtonIconName = mainButtonIconName;
        this.textViewContent = textViewContent;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_recycler_view_content, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CreateSettingsAdapter.ViewHolder holder, final int position) {

        final int itemIndex = holder.getAdapterPosition();
        String mainButIcon = mainButtonIconName;
        int imgResId = getImageId(mainButIcon);
        holder.mainButton.setImageResource(imgResId);

        String textViewText = textViewContent[itemIndex];
        holder.contentTextview.setText(textViewText);

        holder.mainUILayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (itemIndex){
                    case 0:
                        createPincodeSetting();
                        break;
                    case 1:
                        createPatternSetting();
                        break;
                    case 2:
                        createPassfaceSetting();
                        break;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return textViewContent.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ConstraintLayout mainUILayout;
        private TextView contentTextview;
        private CircleImageView mainButton;

        ViewHolder(View itemView) {
            super(itemView);
            mainUILayout = itemView.findViewById(R.id.layout_recycler_view_content);
            contentTextview = itemView.findViewById(R.id.content_textview);
            mainButton = itemView.findViewById(R.id.main_button);
        }
    }

    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }

    /** Method that starts the activity for the creation of new PIN Cod setup
     */
    private void createPincodeSetting(){
        Intent intent = new Intent(context, PinCodeAdminActivity.class);
        ((Activity)context).finish();
        context.startActivity(intent);
    }

    private void createPatternSetting(){
        Intent intent = new Intent(context, SchemeAdminActivity.class);
        ((Activity)context).finish();
        context.startActivity(intent);
    }


    private void createPassfaceSetting(){
        Intent intent = new Intent(context, PassFaceAdminActivity.class);
        ((Activity)context).finish();
        context.startActivity(intent);
    }
}
