package aaa.pfe.auth;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import database.AppViewModel;

/** The class handles the deletion of setups as user needs.
 * It allows the user to delete any of the previously created
 * setups for any of the authentication mechanisms
 * Created by Zauwali S. Paki
 * 25/07/2018
 */
public class DeleteSetups extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_setups);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);
        String mainButtonIconName = "ic_delete_forever";
        String[] textViewContent = new String[]{"PIN Code", "Pattern Lock", "Passfaces"};

        final DeleteSetupsAdapter adapter = new DeleteSetupsAdapter(DeleteSetups.this,
                appViewModel, mainButtonIconName, textViewContent);

        RecyclerView recyclerView = findViewById(R.id.delete_setups_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(DeleteSetups.this, HomeUI.class);
        finish();
        startActivity(intent);
        return true;
    }
}
