package aaa.pfe.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.view.passface.DeletePassfaceConfigs;
import aaa.pfe.auth.view.pincode.DeletePincodeConfigs;
import aaa.pfe.auth.view.schemepattern.DeletePatternConfigs;
import database.AppViewModel;
import database.PassFaceConfigParams;
import database.PatternConfigParams;
import database.PincodeConfigParams;
import de.hdodenhof.circleimageview.CircleImageView;

/** Adapter class for the deletion of configuration setups
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class DeleteSetupsAdapter extends RecyclerView.Adapter<DeleteSetupsAdapter.ViewHolder> {

    private Context context;
    private AppViewModel appViewModel;
    private String[] textViewContent;
    private String mainButtonIconName;
    DeleteSetupsAdapter(Context context, AppViewModel appViewModel,
                        String mainButtonIconName, String[] textViewContent){

        this.context = context;
        this.appViewModel = appViewModel;
        this.mainButtonIconName = mainButtonIconName;
        this.textViewContent = textViewContent;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_recycler_view_content, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int itemIndex = holder.getAdapterPosition();
        String mainButIcon = mainButtonIconName;
        int imgResId = getImageId(mainButIcon);
        holder.mainButton.setImageResource(imgResId);

        String textViewText = textViewContent[itemIndex];
        holder.contentTextview.setText(textViewText);

        holder.mainUILayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (itemIndex){
                    case 0:
                        try {
                            showPincodeSettingsList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 1:
                        try {
                            showPatternSettingsList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            showPassfaceSettinsList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return textViewContent.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ConstraintLayout mainUILayout;
        private TextView contentTextview;
        private CircleImageView mainButton;

        ViewHolder(View itemView) {
            super(itemView);
            mainUILayout = itemView.findViewById(R.id.layout_recycler_view_content);
            contentTextview = itemView.findViewById(R.id.content_textview);
            mainButton = itemView.findViewById(R.id.main_button);
        }
    }

    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }

    private void showPincodeSettingsList() throws ExecutionException, InterruptedException {

        List<PincodeConfigParams> pincodeSetupsList = appViewModel.getAllPincodeSetupsList();

        //if the list of the setups is non-empty, display it
        if (pincodeSetupsList.size() > 0){
            Intent intent = new Intent(context, DeletePincodeConfigs.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }else {
            Toast.makeText(context,"Empty Setups list", Toast.LENGTH_LONG).show();
        }

    }

    private void showPatternSettingsList() throws ExecutionException, InterruptedException {

        List<PatternConfigParams> patternSetupsList = appViewModel.getAllPatternSetupsList();

        if (patternSetupsList.size() > 0){
            Intent intent = new Intent(context, DeletePatternConfigs.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }else {
            Toast.makeText(context,"Empty Setups list", Toast.LENGTH_LONG).show();
        }
    }

    private void showPassfaceSettinsList() throws ExecutionException, InterruptedException {

        List<PassFaceConfigParams> passfaceSetupsList = appViewModel.getAllPassfaceSetupsList();
        if (passfaceSetupsList.size() > 0){
            Intent intent = new Intent(context, DeletePassfaceConfigs.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }else {
            Toast.makeText(context,"Empty Setups list", Toast.LENGTH_LONG).show();
        }
    }

}
