package aaa.pfe.auth;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import database.AppViewModel;
/** This class is use for the preparation and generation of the setups
 * as pdfs so they can be printed before the experiment
 * for the 3 authentication schemes
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class GenerateSetupsAsPdf extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_setups_as_pdf);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        String createPdfButtonIcon = "ic_picture_as_pdf";
        String viewPdfButtonIcon = "ic_visibility";
        String[] textViewContent = new String[]{"Generate Pin Code", "Generate Pattern Lock", "Generate Passfaces",
        "View Pin Code Setups", "View Pattern Setups","View Passfaces Setups"};

        final GenerateSetupsAsPdfAdapter adapter = new GenerateSetupsAsPdfAdapter(
                GenerateSetupsAsPdf.this, appViewModel, createPdfButtonIcon,
                viewPdfButtonIcon, textViewContent);

        RecyclerView recyclerView = findViewById(R.id.generate_pdf_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(GenerateSetupsAsPdf.this, HomeUI.class);
        finish();
        startActivity(intent);
        return true;
    }
}
