package aaa.pfe.auth;

import android.content.Context;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.utils.TemplatePDF;
import database.AppViewModel;
import database.PassFaceConfigParams;
import database.PatternConfigParams;
import database.PincodeConfigParams;
import de.hdodenhof.circleimageview.CircleImageView;

/** The class that handles the task of creation of setups for the
 * three authentication mechanism.
 * Created by Zauwali S. Paki
 * April 2018
 */

public class GenerateSetupsAsPdfAdapter extends RecyclerView.Adapter<GenerateSetupsAsPdfAdapter.ViewHolder> {

    private Context context;
    private AppViewModel appViewModel;
    private String createPdfButtonIcon;
    private String viewPdfButtonIcon;
    private String[] textViewContent;
    GenerateSetupsAsPdfAdapter(Context context, AppViewModel appViewModel, String createPdfButtonIcon,
                               String viewPdfButtonIcon, String[] textViewContent){

        this.context = context;
        this.appViewModel = appViewModel;
        this.createPdfButtonIcon = createPdfButtonIcon;
        this.viewPdfButtonIcon = viewPdfButtonIcon;
        this.textViewContent = textViewContent;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_recycler_view_content, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GenerateSetupsAsPdfAdapter.ViewHolder holder, final int position) {

        final int itemIndex = holder.getAdapterPosition();
        String mainButIcon;
        int imgResId;
        switch (itemIndex){
            case 0:
            case 1:
            case 2:
                mainButIcon = createPdfButtonIcon;
                imgResId = getImageId(mainButIcon);
                holder.mainButton.setImageResource(imgResId);
                break;
            case 3:
            case 4:
            case 5:
                mainButIcon = viewPdfButtonIcon;
                imgResId = getImageId(mainButIcon);
                holder.mainButton.setImageResource(imgResId);
                break;
        }

        String textViewText = textViewContent[itemIndex];
        holder.contentTextview.setText(textViewText);

        holder.mainUILayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (itemIndex){
                    case 0:
                        try {
                            holder.progressBar.setVisibility(View.VISIBLE);
                            generatPincodeSetupsAsPdf();
                            holder.progressBar.setVisibility(View.INVISIBLE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 1:
                        try {

                            holder.progressBar.setVisibility(View.VISIBLE);
                            generatePatternSetupsAsPdf();
                            holder.progressBar.setVisibility(View.INVISIBLE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            holder.progressBar.setVisibility(View.VISIBLE);
                            generatePassfaceSetupsAsPdf();
                            holder.progressBar.setVisibility(View.INVISIBLE);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 3: viewPincodeSetupsAsPdf();
                        break;
                    case 4: viewPatternSetupsAsPdf();
                        break;
                    case 5: viewPassfaceSetupsAsPdf();
                        break;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return textViewContent.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ConstraintLayout mainUILayout;
        private TextView contentTextview;
        private CircleImageView mainButton;
        private ProgressBar progressBar;

        ViewHolder(View itemView) {
            super(itemView);
            mainUILayout = itemView.findViewById(R.id.layout_recycler_view_content);
            contentTextview = itemView.findViewById(R.id.content_textview);
            mainButton = itemView.findViewById(R.id.main_button);
            progressBar = itemView.findViewById(R.id.progress_bar);
            //progressBar.setProgress(100,true);
        }
    }

    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }

    /** Method that starts the activity for the creation of new PIN Cod setup
     */
    private void generatPincodeSetupsAsPdf() throws ExecutionException, InterruptedException {
        List<PincodeConfigParams> pincodeSetups = appViewModel.getAllPincodeSetupsList();
        if (pincodeSetups.size() < 1){
            Toast.makeText(context,context.getString(R.string.empty_setups_list), Toast.LENGTH_SHORT).show();
        }else {
            TemplatePDF templatePDF = new TemplatePDF(context);
            templatePDF.createPincodeSetupsAsPdf(pincodeSetups);
            Toast.makeText(context,context.getString(R.string.pincode_setup_pdf_generated),
                    Toast.LENGTH_SHORT).show();

        }
    }

    private void generatePatternSetupsAsPdf() throws ExecutionException, InterruptedException {
        List<PatternConfigParams> patternSetups = appViewModel.getAllPatternSetupsList();
        if (patternSetups.size() < 1){
            Toast.makeText(context,context.getString(R.string.empty_setups_list), Toast.LENGTH_SHORT).show();
        }else {
            TemplatePDF templatePDF = new TemplatePDF(context);
            templatePDF.createPatternSetupsAsPdf(patternSetups);
            Toast.makeText(context,context.getString(R.string.pattern_setup_pdf_generated),
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void generatePassfaceSetupsAsPdf() throws ExecutionException, InterruptedException {
        List<PassFaceConfigParams> passfaceSetups = appViewModel.getAllPassfaceSetupsList();
        if (passfaceSetups.size() < 1){
            Toast.makeText(context,context.getString(R.string.empty_setups_list), Toast.LENGTH_SHORT).show();
        }else {
            TemplatePDF templatePDF = new TemplatePDF(context);
            templatePDF.createPassfaceSetupsAsPdf(passfaceSetups);
            Toast.makeText(context,context.getString(R.string.passfaces_setup_pdf_generated),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void viewPincodeSetupsAsPdf(){
        File folder = new File(Environment.getExternalStorageDirectory().toString(),
                context.getString(R.string.setups_as_pdf_folder_name));
        File pincode_setups = new File(folder,context.getString(R.string.pincode_setups_file_name) + ".pdf");
        TemplatePDF templatePDF = new TemplatePDF(context.getApplicationContext());
        templatePDF.viewPDF(pincode_setups);

    }


    private void viewPatternSetupsAsPdf(){
        File folder = new File(Environment.getExternalStorageDirectory().toString(),
                context.getString(R.string.setups_as_pdf_folder_name));
        File pattern_setups = new File(folder,context.getString(R.string.pattern_setups_file_name) + ".pdf");
        TemplatePDF templatePDF = new TemplatePDF(context.getApplicationContext());
        templatePDF.viewPDF(pattern_setups);
    }

    private void viewPassfaceSetupsAsPdf(){
        File folder = new File(Environment.getExternalStorageDirectory().toString(),
                context.getString(R.string.setups_as_pdf_folder_name));
        File passface_setups = new File(folder,
                context.getString(R.string.passfaces_setups_file_name) + ".pdf");
        TemplatePDF templatePDF = new TemplatePDF(context.getApplicationContext());
        templatePDF.viewPDF(passface_setups);

    }

}
