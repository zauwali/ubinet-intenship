package aaa.pfe.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;

/** The class that populates the main activity recycler view
 * Created by Zauwali S. Paki
 * July, 2018
 */
public class HomeUIAdapter extends RecyclerView.Adapter<HomeUIAdapter.ViewHolder> {

    private Activity[] activities;
    private Context context;
    private String[] mainButtonIconName, textViewContent;
    HomeUIAdapter(Context context, String[] mainButtonIconName, String[] textViewContent,
                  Activity[] activities){

        this.context = context;
        this.mainButtonIconName = mainButtonIconName;
        this.textViewContent = textViewContent;
        this.activities = activities;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_home_ui_content, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final int itemIndex = holder.getAdapterPosition();
        String mainButIcon = mainButtonIconName[itemIndex];
        int imgResId = getImageId(mainButIcon);
        holder.mainButton.setImageResource(imgResId);

        String textViewText = textViewContent[itemIndex];
        holder.contentTextview.setText(textViewText);

        holder.mainUILayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, activities[itemIndex].getClass());
                ((Activity)context).finish();
                context.startActivity(intent);
            }
        });

        holder.infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (itemIndex){
                    case 0:
                        Toast.makeText(context, "Create Setups", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(context, "Run Experiments", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(context, "Manage User Ids", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mainButtonIconName.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ConstraintLayout mainUILayout;
        private TextView contentTextview;
        private CircleImageView mainButton, infoButton;

        ViewHolder(View itemView) {
            super(itemView);
            mainUILayout = itemView.findViewById(R.id.layout_home_ui_content);
            contentTextview = itemView.findViewById(R.id.content_textview);
            infoButton = itemView.findViewById(R.id.info_button);
            mainButton = itemView.findViewById(R.id.main_button);
        }
    }

    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }
}
