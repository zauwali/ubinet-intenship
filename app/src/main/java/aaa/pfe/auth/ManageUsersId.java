package aaa.pfe.auth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

/** The class handles management  of user ids
 * Created by Zauwali S. Paki
 * 23/07/2018
 */
public class ManageUsersId extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_users_id);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(ManageUsersId.this, HomeUI.class);
        finish();
        startActivity(intent);
        return true;
    }
}
