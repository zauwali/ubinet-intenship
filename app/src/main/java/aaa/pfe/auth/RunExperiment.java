package aaa.pfe.auth;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import database.AppViewModel;

/** The activity for running experiments for the various
 * authentication mechanisms
 * Created by Zauwali S. Paki
 * May 2018
 */
public class RunExperiment extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_experiment);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Run Experiments");

        String[] mainButtonIconNames = new String[]{"ic_pincode_pad","ic_pattern","ic_photo_library"};
        String[] textViewContent = new String[]{"Pin Code", "Pattern Lock", "Pass Faces"};

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        final RunExperimentAdapter adapter = new RunExperimentAdapter(RunExperiment.this, appViewModel, mainButtonIconNames,
                textViewContent);
        RecyclerView recyclerView = findViewById(R.id.run_experiment_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(RunExperiment.this, HomeUI.class);
        finish();
        startActivity(intent);
        return true;
    }
}
