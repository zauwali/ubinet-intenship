package aaa.pfe.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.view.passface.PassFaceAdminActivity;
import aaa.pfe.auth.view.passface.PassfaceConfigsList;
import aaa.pfe.auth.view.pincode.PinCodeAdminActivity;
import aaa.pfe.auth.view.pincode.PincodeConfigParamsList;
import aaa.pfe.auth.view.schemepattern.PatternConfigs;
import aaa.pfe.auth.view.schemepattern.SchemeAdminActivity;
import database.AppViewModel;
import database.PassFaceConfigParams;
import database.PatternConfigParams;
import database.PincodeConfigParams;
import de.hdodenhof.circleimageview.CircleImageView;

/** The class that maintains the list of setups that have been created.
 * If the list of a given setup is empty, user is prompted if he/she wants
 * to create a setup
 * Author: Zauwali S. Paki
 * April 2018
 */

public class RunExperimentAdapter extends RecyclerView.Adapter<RunExperimentAdapter.ViewHolder> {

    private AppViewModel appViewModel;
    private Context context;
    private String[] mainButtonIconName, textViewContent;
    RunExperimentAdapter(Context context, AppViewModel appViewModel,
                         String[] mainButtonIconName, String[] textViewContent){

        this.context = context;
        this.appViewModel = appViewModel;
        this.mainButtonIconName = mainButtonIconName;
        this.textViewContent = textViewContent;

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_recycler_view_content, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final int itemIndex = holder.getAdapterPosition();
        String mainButIcon = mainButtonIconName[itemIndex];
        int imgResId = getImageId(mainButIcon);
        holder.mainButton.setImageResource(imgResId);

        String textViewText = textViewContent[itemIndex];
        holder.contentTextview.setText(textViewText);

        holder.mainUILayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (itemIndex){
                    case 0:
                        try {
                            showPincodeSettingsList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 1:
                        try {
                            showPatternSettingsList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case 2:
                        try {
                            showPassfaceSettinsList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mainButtonIconName.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ConstraintLayout mainUILayout;
        private TextView contentTextview;
        private CircleImageView mainButton;

        ViewHolder(View itemView) {
            super(itemView);
            mainUILayout = itemView.findViewById(R.id.layout_recycler_view_content);
            contentTextview = itemView.findViewById(R.id.content_textview);
            mainButton = itemView.findViewById(R.id.main_button);
        }
    }

    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }

    /** A method that displays list of PIN code setups previously saved in the database
     */
    private void showPincodeSettingsList() throws ExecutionException, InterruptedException {

        List<PincodeConfigParams> pincodeSetupsList = appViewModel.getAllPincodeSetupsList();

        //if the list of the setups is non-empty, display it
        if (pincodeSetupsList.size() > 0){
            Intent intent = new Intent(context, PincodeConfigParamsList.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }else {
            Alert.emptySetupsList(context, new PinCodeAdminActivity());
        }

    }

    private void showPatternSettingsList() throws ExecutionException, InterruptedException {

        List<PatternConfigParams> patternSetupsList = appViewModel.getAllPatternSetupsList();

        if (patternSetupsList.size() > 0){
            Intent intent = new Intent(context, PatternConfigs.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }else {
            Alert.emptySetupsList(context,new SchemeAdminActivity());
        }
    }

    private void showPassfaceSettinsList() throws ExecutionException, InterruptedException {

        List<PassFaceConfigParams> passfaceSetupsList = appViewModel.getAllPassfaceSetupsList();
        if (passfaceSetupsList.size() > 0){
            Intent intent = new Intent(context, PassfaceConfigsList.class);
            ((Activity)context).finish();
            context.startActivity(intent);
        }else {
            Alert.emptySetupsList(context, new PassFaceAdminActivity());
        }
    }
}

