package aaa.pfe.auth.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import aaa.pfe.auth.R;

/** Class that handles the various alert dialogs that app displays
 * to the user.
 * Created by Zauwali S. Paki
 * June 2018
 */
public final class Alert {

    public static void showWrongPatternMsg(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.wrong_pattern);
        alert.setIcon(R.drawable.ic_error_red);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    });
        alert.create().show();
}

    public static  void showCorrectPatternMsg(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.correct_pattern);
        alert.setIcon(R.drawable.ic_info_teal);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.create().show();
    }

    public static void showWrongPassfaceMsg(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.wrong_passface);
        alert.setIcon(R.drawable.ic_error_red);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.create().show();
    }

    public static  void showCorrectPassfaceMsg(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.correct_passface);
        alert.setIcon(R.drawable.ic_info_teal);
        alert.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.create().show();
    }

    public static  void showWrongPinMsg(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.wrong_pin);
        alert.setIcon(R.drawable.ic_error_red);
        alert.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.create().show();

    }

    public static void showCorrectPinMsg(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.correct_pin);
        alert.setIcon(R.drawable.ic_info_teal);
        alert.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alert.create().show();
    }

    public static  void configSavedMsg(final Context context, final Activity[] activity){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(R.string.config_setting_saved_message_title);
        alert.setIcon(R.drawable.ic_info_teal);
        alert.setMessage(R.string.config_setting_saved_message);
        alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, activity[0].getClass());
                ((Activity)context).finish();
                context.startActivity(intent);
            }
        });
        alert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, activity[1].getClass());
                ((Activity)context).finish();
                context.startActivity(intent);
            }
        });
        alert.create().show();

    }
    public static void maxAttemptsMsg(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.maximum_attempts_exceeded);
        builder.setIcon(R.drawable.ic_warning_teal_dark);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create().show();
    }

    public static void emptySetupsList(final Context context, final Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.empty_setups_list);
        builder.setMessage(R.string.do_u_want_create_setup);
        builder.setIcon(R.drawable.ic_warning_teal_dark);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, activity.getClass());
                ((Activity)context).finish();
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }
}
