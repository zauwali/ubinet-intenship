package aaa.pfe.auth.utils;

/** This class some constant variables used by the app
 * for the 3 authentication schemes
 * Modified by: Zauwali S. Paki
 * April 2018
 * */
public class Const {
    public static final int DEFAULT_PASSFACE_SIZE = 4;
    public static final int DEFAULT_PASSWORD_LENGTH = 1;
    public static final int DEFAULT_NUM_PHOTOS = 5;
    public static final int DEFAULT_NUM_ATTEMPTS = 3;
    public static final boolean DEFAULT_ORDER = true;
    public static final boolean DEFAULT_SHUFFLE = true;
    public static final String DEFAULT_MATCHING = "Exact";
    public static final String DEFAULT_TYPE_PHOTOS = "Misc";
    public static final boolean DEFAULT_USE_PHOTO_MULTIPLE_TIMES = false;

    public final static String[] TREES = new String[]{
            "tree_apple", "tree_apricot", "tree_avocado", "tree_banana", "tree_blueberry","tree_cacoa",
            "tree_cashew","tree_date", "tree_grapes", "tree_guanabana", "tree_guava", "tree_kiwi",
            "tree_lychee", "tree_mango", "tree_orange", "tree_pawpaw", "tree_peach", "tree_pear","tree_pepino",
            "tree_pineapple", "tree_princess", "tree_strawberry", "tree_sugar_apple", "tree_sweetberry"};

    public final static String[] ANIMALS = new String[]{
            "anim_bear", "anim_bison", "anim_camel", "anim_cat", "anim_cheetah", "anim_chimpanzee", "anim_deer",
            "anim_dog", "anim_elephant","anim_girrafe", "anim_goat", "anim_hippopotamus", "anim_horse", "anim_hyena", "anim_jaguar",
            "anim_kangaroo", "anim_leopard", "anim_lion","anim_monkey", "anim_panda", "anim_pig","anim_rhineceros",
            "anim_tiger", "anim_zebra"};

    public final static String[] FLOWERS = new String[]{
            "flower_0", "flower_1", "flower_2", "flower_3", "flower_4", "flower_5", "flower_6",
            "flower_7", "flower_8", "flower_9", "flower_10", "flower_11", "flower_12", "flower_13", "flower_14",
            "flower_15", "flower_16", "flower_17", "flower_18", "flower_19", "flower_20", "flower_21", "flower_22"
            , "flower_23", "flower_24"};

    public final static String[] NATURE = new String[]{
            "nature_0", "nature_1", "nature_2", "nature_3", "nature_4", "nature_5", "nature_6",
            "nature_7", "nature_8", "nature_9", "nature_10", "nature_11", "nature_12", "nature_13", "nature_14",
            "nature_15", "nature_16", "nature_17", "nature_18", "nature_19", "nature_20", "nature_21", "nature_22"
            , "nature_23", "nature_24"};
    public final static String[] MISC = new String[]{
            "anim_dog", "flower_0", "nature_0", "tree_apple", "anim_cat", "flower_1", "nature_1","tree_apricot",
            "anim_bear", "flower_2", "nature_2", "tree_avocado", "anim_camel", "flower_3", "nature_3","tree_banana",
            "anim_cheetah", "flower_4", "nature_4", "tree_blueberry", "anim_chimpanzee", "flower_5","nature_5", "tree_cacao"};
}
