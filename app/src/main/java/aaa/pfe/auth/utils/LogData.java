package aaa.pfe.auth.utils;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**The class responsible for writing app logs
 * Created by Anasse on 29/11/2017.
 * Modified by Zauwali S. Paki on 20/07/2018
 */

public class LogData {

    private static String directoryName = "/Android Auth Mechs Log Files/";
    private String fileName;

    // Delimiter used in the csv file
    private static final String COMMA_DELIMETER = ",";
    private static final String NEW_LINE_SEPARATER = "\n";
    private static final String FILE_HEADER = "Config Id, User Id, User Actions, Attempt, Time, Date";

    //parameters of the log
    String configId, userId, action, time, date, attempt;

//    public LogData(String method){
//        DateFormat df = new SimpleDateFormat("dd MM yyyy_ HH_mm");
//        String date = df.format(Calendar.getInstance().getTime());
//        date = date.replace(" ","_");
//        this.fileName = method + "_" + date + ".csv";
//    }

    public LogData(String configId, String userId, String action,
                   String attempt,String time, String date){

        this.configId = configId;
        this.userId = userId;
        this.action = action;
        this.time = time;
        this.date = date;
        this.attempt = attempt;
    }

    public static boolean canWriteOnExternalStorage() {
        // get the state of your external storage
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // if storage is mounted return true
            return true;
        }
        return false;
    }

//    public void logParams(ArrayList<String> values){
//
//        try {
//            writeInFile("Params;");
//            for (int i = 0; i < values.size(); i++) {
//                writeInFile(values.get(i)+";");
//            }
//            writeInFile("\n");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public void logNextTentative(ArrayList values){
//        try {
//            writeInFile(";");
//            for (int i = 0; i < values.size(); i++) {
//                writeInFile(values.get(i)+";");
//            }
//            writeInFile("\n");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public void writeInFile(String txt) throws IOException {
//
//        if(canWriteOnExternalStorage()) {
//
//            File sdcard = Environment.getExternalStorageDirectory();
//            File dir = new File(sdcard.getAbsolutePath() + directoryName);
//            dir.mkdirs();
//            File file = new File(dir, fileName);
//            String data = txt;
//            if (!file.exists()) {
//                file.createNewFile();
//                FileWriter heapFileWritter = new FileWriter(
//                        file.getAbsolutePath(), true);
//                BufferedWriter heapBufferWritter = new BufferedWriter(
//                        heapFileWritter);
//                heapBufferWritter.write(data);
//                heapBufferWritter.close();
//
//            }else{
//                FileWriter heapFileWritter = new FileWriter(
//                        file.getAbsolutePath(), true);
//                BufferedWriter heapBufferWritter = new BufferedWriter(
//                        heapFileWritter);
//                heapBufferWritter.write(data);
//                heapBufferWritter.close();
//
//            }
//        }
//    }

    public static void writeLog(ArrayList<LogData> logs, String fileName) throws IOException{
        //this.fileName = filename;

        if(canWriteOnExternalStorage()) {

            File sdcard = Environment.getExternalStorageDirectory();
            File dir = new File(sdcard.getAbsolutePath() + directoryName);
            dir.mkdirs();
            File file = new File(dir, fileName);
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWriter = new FileWriter(
                        file.getAbsolutePath(), true);
                BufferedWriter bufferedWriter = new BufferedWriter(
                        fileWriter);

                bufferedWriter.write(FILE_HEADER);
                bufferedWriter.write(NEW_LINE_SEPARATER);

                for(LogData log: logs){
                    bufferedWriter.write(log.configId);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.userId);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.action);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.attempt);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.time);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.date);
                    bufferedWriter.write(NEW_LINE_SEPARATER);
                }
                bufferedWriter.close();

            }else{
                FileWriter fileWriter = new FileWriter(
                        file.getAbsolutePath(), true);
                BufferedWriter bufferedWriter = new BufferedWriter(
                        fileWriter);

                for(LogData log: logs){
                    bufferedWriter.write(log.configId);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.userId);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.action);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.attempt);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.time);
                    bufferedWriter.write(COMMA_DELIMETER);
                    bufferedWriter.write(log.date);
                    bufferedWriter.write(NEW_LINE_SEPARATER);
                }
                bufferedWriter.close();
            }
        }
    }
}
