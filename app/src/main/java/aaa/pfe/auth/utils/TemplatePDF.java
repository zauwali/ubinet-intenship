package aaa.pfe.auth.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import aaa.pfe.auth.GenerateSetupsAsPdf;
import aaa.pfe.auth.R;
import database.PassFaceConfigParams;
import database.PatternConfigParams;
import database.PincodeConfigParams;
/** This class is used to prepare setups as pdfs
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class TemplatePDF extends GenerateSetupsAsPdf{
    private Context context;
    private File pdfFile;
    private Document document;
    private PdfWriter pdfWriter;
    private Paragraph paragraph;
    private Font fTitle = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private Font fSubTitle = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private Font fText = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);
    private Font fHighText = new Font(Font.FontFamily.TIMES_ROMAN, 15, Font.BOLD, BaseColor.RED);

    public TemplatePDF(Context context) {
        this.context = context;
    }

    public void openDocument(String fileName){
        createFile(fileName);
        try{
            document = new Document(PageSize.A4);
            document.addCreationDate();
            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
            document.open();
        }catch (Exception e){}
    }

    private void createFile(String fileName){
        File folder = new File(Environment.getExternalStorageDirectory().toString(),
                context.getString(R.string.setups_as_pdf_folder_name));
        if (!folder.exists())
            folder.mkdir();
        pdfFile = new File(folder, fileName + ".pdf");

    }

    public void closeDocument(){
        document.close();
    }

    public void addMetaDate(String title, String subject, String author){
        document.addTitle(title);
        document.addSubject(subject);
        document.addAuthor(author);
        document.addCreationDate();
    }

    public void addTitles(String title, String subTitle, String date){
        try {
            paragraph = new Paragraph();
            addChildP(new Paragraph(title, fTitle));
            addChildP(new Paragraph(subTitle, fSubTitle));
            addChildP(new Paragraph(date, fHighText));
            paragraph.setSpacingAfter(30);
            document.add(paragraph);
        }catch (Exception e){
        }
    }

    public void addChildP(Paragraph childParagraph){
        childParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(childParagraph);
    }

    public void addParagraph(String text){
        try {
            paragraph = new Paragraph(text, fText);
            paragraph.setSpacingAfter(5);
            paragraph.setSpacingBefore(5);
            document.add(paragraph);
        }catch (Exception e){
        }
    }

    public void createTable(String[] header, ArrayList<String[]> rows){

        try {
            paragraph = new Paragraph();
            paragraph.setFont(fText);
            PdfPTable pdfPTable = new PdfPTable(header.length);
            pdfPTable.setWidthPercentage(60);
            PdfPCell pdfPCell;
            int indexC = 0;
            while (indexC < header.length){
                pdfPCell = new PdfPCell(new Phrase(header[indexC++], fSubTitle));
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setBackgroundColor(BaseColor.GREEN);
                pdfPTable.addCell(pdfPCell);
            }

            for (int indexR = 0; indexR < rows.size(); indexR++){
                String[] row = rows.get(indexR);
                for (indexC = 0; indexC < header.length; indexC++){
                    pdfPCell = new PdfPCell(new Phrase(row[indexC]));
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    //pdfPCell.setFixedHeight(30);
                    pdfPTable.addCell(pdfPCell);
                }
            }

            paragraph.add(pdfPTable);
            document.add(paragraph);

        }catch (Exception e){
        }
    }

    private void createPatternTable(String[] tableHeading,StringBuilder setupInfo,Bitmap patternPhoto){
        try {
            paragraph = new Paragraph();
            paragraph.setFont(fText);
            PdfPTable pdfPTable = new PdfPTable(tableHeading.length);
            pdfPTable.setWidthPercentage(80);
            PdfPCell pdfPCell;
            int indexC = 0;
            while (indexC < tableHeading.length){
                pdfPCell = new PdfPCell(new Phrase(tableHeading[indexC++], fSubTitle));
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setBackgroundColor(BaseColor.GREEN);
                pdfPTable.addCell(pdfPCell);
            }


            //for adding the setup info
            pdfPCell = new PdfPCell(new Phrase(String.valueOf(setupInfo)));
            pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfPTable.addCell(pdfPCell);

            //for adding the pattern image
            Bitmap patternImage = patternPhoto;
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            patternImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] image2ByteArray = stream.toByteArray();
            Image image = Image.getInstance(image2ByteArray);
            pdfPCell = new PdfPCell(image,true);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPTable.addCell(pdfPCell);

            paragraph.add(pdfPTable);
            document.add(paragraph);

        }catch (Exception e){
        }

    }

    private void createPassfaceTable(StringBuilder setupInfo, ArrayList<String> passfacePhotoNames){

        PdfPTable pdfPTable;
        PdfPCell pdfPCell;

        try {
            paragraph = new Paragraph();
            paragraph.setFont(fText);

            pdfPTable = new PdfPTable(1);
            pdfPTable.setWidthPercentage(100);
            pdfPCell = new PdfPCell(new Phrase(String.valueOf(setupInfo)));
            pdfPTable.addCell(pdfPCell);

            pdfPCell = new PdfPCell(new Phrase("Passfaces Images"));
            pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPCell.setBackgroundColor(BaseColor.GREEN);
            pdfPTable.addCell(pdfPCell);
            paragraph.add(pdfPTable);

            pdfPTable = new PdfPTable(passfacePhotoNames.size());
            pdfPTable.setWidthPercentage(100);

            for (String image: passfacePhotoNames){

                // getting the image from the drawable folder
                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), getImageId(image));
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                byte[] image2ByteArray = bos.toByteArray();
                Image photo = Image.getInstance(image2ByteArray);
                pdfPCell = new PdfPCell(photo,true);

                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPTable.addCell(pdfPCell);
            }

            paragraph.add(pdfPTable);
            document.add(paragraph);

        }catch (Exception e){
        }
    }

    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }

    public void viewPDF(File fileName){
        if (fileName.exists()){
            Intent intent = new Intent(context, ViewPDF.class);
            intent.putExtra("path",fileName.getAbsolutePath());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);}
        else {
            Toast.makeText(context,"The file does not exist",Toast.LENGTH_LONG).show();
        }


    }

    public void appViewPdf(File fileName, Activity activity){
        if (fileName.exists()){
            Uri uri = Uri.fromFile(fileName);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri,"application/pdf");
            try {
                activity.startActivity(intent);
            }catch (ActivityNotFoundException e){
                activity.startActivity(new Intent(intent.ACTION_VIEW,Uri.parse("market://details?id=com.adobe.reader")));
                Toast.makeText(activity.getApplicationContext(),"No app to open pdf", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(activity.getApplicationContext(),"No app to open pdf", Toast.LENGTH_LONG).show();
        }
    }

    public void createPincodeSetupsAsPdf(List<PincodeConfigParams> pincodeSetups){
        //create and open the file and folder if not already existing
        openDocument("pincode_setups");
        addTitles("Pincode Setups for the experimentation",
                "Setups and their details", TimeAndDate.getCurrentDate());
        addMetaDate("Pincode Setups for the experimentaion", "Setups Details", "Zauwali S. Paki");

        for (int i = 0; i < pincodeSetups.size(); i++){

            PincodeConfigParams currentSetup = pincodeSetups.get(i);
            String[] tableHeading =  new String[]{"Setup Info", "Value"};
            ArrayList<String[]> rows = new ArrayList<>();
            rows.add(new String[]{context.getString(R.string.setup_id), String.valueOf(currentSetup.getConfigId())});
            rows.add(new String[]{context.getString(R.string.pincode_length), String.valueOf(currentSetup.getPincodeLength())});
            rows.add(new String[]{context.getString(R.string.pincode_pin), currentSetup.getPinCode()});
            rows.add(new String[]{context.getString(R.string.pincode_num_attempt), String.valueOf(currentSetup.getMaxTrials())});
            rows.add(new String[]{context.getString(R.string.pincode_length), String.valueOf(currentSetup.getPincodeLength())});
            rows.add(new String[]{context.getString(R.string.pincode_enable_random_pad), currentSetup.isShufflePad()? "On" : "Off"});
            rows.add(new String[]{context.getString(R.string.pincode_no_indicator), currentSetup.isNoIndicator()? "On" : "Off"});
            rows.add(new String[]{context.getString(R.string.pincode_show_dots_fill), currentSetup.isDotsToFill()? "On" : "Off"});
            rows.add(new String[]{context.getString(R.string.pincode_show_last_number_entered), currentSetup.isShowLastNum()? "On" : "Off"});
            rows.add(new String[]{context.getString(R.string.pincode_show_appearing_dots), currentSetup.isAppearingDots()? "Of" : "Off" + "\n"} );

            createTable(tableHeading,rows);
        }
        closeDocument();
    }

    public void createPatternSetupsAsPdf(List<PatternConfigParams> patternSetups){
        //create and open the file and folder if not already created
        openDocument("pattern_setups");
        addTitles("Pattern Setups for the experimentation",
                "Setups and their details", "Generated on " + TimeAndDate.getCurrentDate());

        for (int i = 0; i < patternSetups.size(); i++) {
            PatternConfigParams currentSetup = patternSetups.get(i);

            StringBuilder setupInfo = new StringBuilder();
            setupInfo.append(context.getString(R.string.pattern_setup_id) + ": " + String.valueOf(currentSetup.getConfigId()+ "\n"));
            setupInfo.append(context.getString(R.string.pattern_num_rows) + ": " + String.valueOf(currentSetup.getNumRows()+ "\n"));
            setupInfo.append(context.getString(R.string.pattern_num_cols) + ": " + String.valueOf(currentSetup.getNumCols()+ "\n"));
            setupInfo.append(context.getString(R.string.pattern_pattern_length) + ": " + String.valueOf(currentSetup.getPatternLength()+ "\n"));
            setupInfo.append(context.getString(R.string.pattern_num_tries) + ": " + String.valueOf(currentSetup.getMaxTrials()+ "\n"));
            setupInfo.append(context.getString(R.string.pattern_size_of_dots) + ": " + String.valueOf(currentSetup.getDotsSize()+ "\n"));
            setupInfo.append(context.getString(R.string.pattern_is_vibration_on) + ": " + String.valueOf(currentSetup.isVibration()? "On" : "Off") + "\n");
            setupInfo.append(context.getString(R.string.pattern_is_stealth_on) + ": " + String.valueOf(currentSetup.isStealth()? "On" : "Off") + "\n");

            Bitmap patternImage = currentSetup.getPatternImage();
            String[] tableHeading = {"Setup Info", "Image of the Pattern"};

            createPatternTable(tableHeading, setupInfo, patternImage);
        }

        document.close();
        //hideProgressBar();

    }

    public void createPassfaceSetupsAsPdf(List<PassFaceConfigParams> passfaceSetups){
        openDocument("passfaces_setups");
        addTitles("Passface Setups for the experimentation",
                "Setups and their details", "Generated on " + TimeAndDate.getCurrentDate());

        for (int i = 0; i < passfaceSetups.size(); i++){
            PassFaceConfigParams currentSetup = passfaceSetups.get(i);
            StringBuilder setupInfo = new StringBuilder();
            setupInfo.append(context.getString(R.string.passface_setup_id) + ": " + String.valueOf(currentSetup.getConfigId()) + "\n");
            setupInfo.append(context.getString(R.string.passface_num_tiles) + ": " + String.valueOf(currentSetup.getNumPhotosShown()) + "\n");
            setupInfo.append(context.getString(R.string.passface_type_photos) + ": " + currentSetup.getTypePhoto() + "\n");
            setupInfo.append(context.getString(R.string.passface_size) + ": " + String.valueOf(currentSetup.getNumPhotos()) + "\n");
            setupInfo.append(context.getString(R.string.passface_num_tries) + ": " + String.valueOf(currentSetup.getMaxTrials()) + "\n");
            setupInfo.append(context.getString(R.string.passface_shuffle_photos) + ": " + String.valueOf(currentSetup.isShufflePhotos()? "Yes" : "No" )+ "\n");
            setupInfo.append(context.getString(R.string.passface_use_same_photo_multiple_times) + ": " + String.valueOf(currentSetup.isUsePhotoMultipleTimes()? "Yes" : "No" ));

            createPassfaceTable(setupInfo, currentSetup.getPassword());
        }

        document.close();
    }


}
