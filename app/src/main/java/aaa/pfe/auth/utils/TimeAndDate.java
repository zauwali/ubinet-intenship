package aaa.pfe.auth.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
/** This is a utility class for getting time and date info
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class TimeAndDate {

    public static String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);

//        return LocalDateTime.now()
//                .format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public static String getCurrentTimeAndDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);

        //DateFormat df = new SimpleDateFormat("HH:MM:SS DD/MM/YYYY");
        //String date2 = df.format(Calendar.getInstance().getTime());
        //System.out.println(formatter.format(date));
        //return date2;

//        return LocalDateTime.now()
//                .format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
    }

    public static String getCurrentDate(){

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return formatter.format(date);
//        return LocalDateTime.now()
//                .format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }
}
