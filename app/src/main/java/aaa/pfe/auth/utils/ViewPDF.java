package aaa.pfe.auth.utils;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.util.ArrayList;

import aaa.pfe.auth.GenerateSetupsAsPdf;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.utils.TemplatePDF;
import aaa.pfe.auth.utils.TimeAndDate;

/** This is used to view pdf files of the generated setups
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class ViewPDF extends AppCompatActivity {

    private PDFView pdfView;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pdfView = findViewById(R.id.pdfView);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            file = new File(bundle.getString("path", ""));
        }
        pdfView.fromFile(file)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .enableAntialiasing(true)
                .load();

    }

    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(ViewPDF.this, GenerateSetupsAsPdf.class);
        finish();
        startActivity(intent);
        return true;
    }

}
