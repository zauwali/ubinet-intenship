package aaa.pfe.auth.view.passface;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import aaa.pfe.auth.DeleteSetups;
import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PassFaceConfigParams;
/** This class for the deletion of passfaces setups
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class DeletePassfaceConfigs extends AppCompatActivity {

    private List<PassFaceConfigParams> passfaceSeupts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_passface_configs);


        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        final DeletePassfaceConfigsAdapter adapter = new DeletePassfaceConfigsAdapter(DeletePassfaceConfigs.this);
        final RecyclerView recyclerView = findViewById(R.id.image_list_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final TextView titleTextView = findViewById(R.id.title_textview);
        final TextView empty_view = findViewById(R.id.empty_view);

        appViewModel.getAllFacePassConfigParams().observe(this, new Observer<List<PassFaceConfigParams>>() {
            @Override
            public void onChanged(@Nullable List<PassFaceConfigParams> passFaceConfigParams) {
                adapter.setConfigurations(passFaceConfigParams);

                passfaceSeupts = passFaceConfigParams;
                if (passfaceSeupts.size()  == 0){
                    titleTextView.setVisibility(View.GONE);
                    empty_view.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else {
                    titleTextView.setVisibility(View.VISIBLE);
                    empty_view.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(DeletePassfaceConfigs.this, DeleteSetups.class);
        finish();
        startActivity(intent);
        return true;
    }
}
