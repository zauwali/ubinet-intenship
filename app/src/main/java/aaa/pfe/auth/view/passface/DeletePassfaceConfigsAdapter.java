package aaa.pfe.auth.view.passface;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PassFaceConfigParams;

/** Adapter class for DeletePassfaceConfigs
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class DeletePassfaceConfigsAdapter extends RecyclerView.Adapter<DeletePassfaceConfigsAdapter.ViewHolder> {

    private Context context;

    private List<PassFaceConfigParams> mPassfaceConfigParams;
    private Application application;
    private AppViewModel appViewModel;

    DeletePassfaceConfigsAdapter(Context context){
        this.context = context;
        application = new Application();
        appViewModel = new AppViewModel(application);
    }
    @NonNull
    @Override
    public DeletePassfaceConfigsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_passface_configs_content, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeletePassfaceConfigsAdapter.ViewHolder holder, final int position) {

        final PassFaceConfigParams configParams = mPassfaceConfigParams.get(position);

        final int numPhotosShown = configParams.getNumPhotosShown();
        final int configId = configParams.getConfigId();
        final int numPhotos = configParams.getNumPhotos();
        final int numAttempts = configParams.getMaxTrials();
        final String typePhotos = configParams.getTypePhoto();
        final String shufflePhotos = configParams.isShufflePhotos()? "YES" : "NO";
        String usePhotoMultipleTimes = configParams.isUsePhotoMultipleTimes()? "YES" : "NO";

        StringBuilder builder = new StringBuilder();
        builder.append("Setup Id: " + configId + "\n");
        builder.append("Number of Photos: " + numPhotosShown + "\n");
        builder.append("Type of Photos: " + typePhotos + "\n");
        builder.append("Number of Photos for password: " + numPhotos + "\n");
        builder.append("Number of Attempts Allowed: " + numAttempts + "\n");
        builder.append("Shuffle Photos: " + shufflePhotos + "\n");
        builder.append("Use Same Photo Multiple Times: " + usePhotoMultipleTimes);

        // gett the list of the names of images that make up the password
        ArrayList<String> passfacePassword = configParams.getPassword();

        final StringBuilder passwordArray = new StringBuilder();

        for (int i = 0; i < passfacePassword.size(); i++){
            if (i == passfacePassword.size()-1){
                passwordArray.append(passfacePassword.get(i));
            }else {
                passwordArray.append(passfacePassword.get(i)).append("+");
            }
        }

        holder.textView.setText(builder);

        //Resize the image view
        for (int i = 0; i < passfacePassword.size(); i++){
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) holder.images[i].getLayoutParams();
            params.height = 180;
            params.width = 180;
            holder.images[i].setLayoutParams(params);
        }

        // If the number of the images making up the passface password is less than the number image views,
        // the extra image view are flatten by by setting their widths and heights to zero so they do not
        // occupy space on the screen.
        if (holder.images.length >= passfacePassword.size()){
            for (int i = passfacePassword.size(); i < holder.images.length; i++){
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) holder.images[i].getLayoutParams();
                params.height = 0;
                params.width = 0;
                holder.images[i].setLayoutParams(params);

            }
        }
        //for displaying the images on the image views
        for (int i = 0; i < passfacePassword.size(); i++){
            String currentImage = passfacePassword.get(i);
            int imgResId = getImageId(currentImage);
            holder.images[i].setImageResource(imgResId);
        }

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Deleting a Passface Config");
                alert.setMessage("Do you want to delete the selected config?");
                alert.setIcon(R.drawable.ic_warning_teal_dark);
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appViewModel.deletePassfaceConfig(configParams.getConfigId());
                        Toast.makeText(context,"Config Setup deleted", Toast.LENGTH_LONG).show();
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = alert.create();
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mPassfaceConfigParams == null){
            return 0;
        }else {
            return mPassfaceConfigParams.size();
        }
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout constraintLayout;
        private TextView textView;
        private ImageView[] images = new ImageView[16];
        ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.title_textview);
            constraintLayout = itemView.findViewById(R.id.layout_passface_configs_content);

            for(int i = 0; i < images.length; i++){
                String imageId = "image_" + i;
                int resId = context.getResources().getIdentifier(imageId,"id", context.getPackageName());
                images[i] = itemView.findViewById(resId);
            }
        }
    }
    /** Method for getting the identifier for the image
     * @param imageName: the name of the image for which the id is to be discovered
     * @return it returns the integer id of the given image name
     */
    private int getImageId(String imageName){
        return context.getResources().getIdentifier(
                "drawable/" + imageName,null, context.getPackageName());
    }

    void setConfigurations(List<PassFaceConfigParams> configurations){
        mPassfaceConfigParams = configurations;
        notifyDataSetChanged();
    }

}
