package aaa.pfe.auth.view.passface;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.RunExperiment;
import aaa.pfe.auth.utils.Const;
import aaa.pfe.auth.utils.LogData;
import aaa.pfe.auth.utils.TimeAndDate;
import database.AppViewModel;
import database.PassfacesUserId;
import de.hdodenhof.circleimageview.CircleImageView;

/** The activity for the passfaces experiment
 * Updated by: Zauwali S. Paki
 * April 2018
 * */

public class PassFaceActivity extends AppCompatActivity implements
        View.OnClickListener, AdapterView.OnItemClickListener{


    //View attributes
    private GridView gridView;
    private SharedPreferences sharedPreferences;

    // savedPasswordToArray variable is used to split the saved password into names of images that were chosen
    // during the creation of the configuration. The image names were concatenated together for
    // saving usin + operator. It is a convenient variable
    private String[] savedPasswordToArray;
    private ArrayList<String> enteredPassfacePassword = new ArrayList<>();
    //we use this variable to copy the saved password
    private ArrayList<String> savedPassfacePassword = new ArrayList<>();
    private List<ImageView> listPhotosChosen;
    private int lengthOfCurrentPWD = 0;
    private int currentStep = 0;

    // used to know how to update the gridview
    private int currentNumAttempt = 1;

    //Different parameters
    private int numPhotosShown;
    private int numPhotos;
    private int numAttempts;
    //private boolean orderOfPwd;
    private boolean shufflePhotos;
    private boolean usePhotoMultipleTimes;

    int userId, configId;

    private ArrayList<LogData> logs = new ArrayList<>();

    private TextView configIdTextView, setupIdLabelTextview;
    //private Spinner userIdSpinner;

    private CircleImageView startExperiment;
    private AppViewModel appViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_face);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        configIdTextView = findViewById(R.id.setup_id_textview);
        setupIdLabelTextview = findViewById(R.id.setup_id_label_textview);

        appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        CircleImageView resetPassword = findViewById(R.id.reset_password);
        resetPassword.setOnClickListener(this);

        gridView = findViewById(R.id.gridView);
        gridView.setOnItemClickListener(this);

        /*
        userIdSpinner = findViewById(R.id.users_id_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.users_id, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userIdSpinner.setAdapter(adapter);
         */

        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.passFacePreferences), MODE_PRIVATE);

//        Thread t = new Thread(){
//            @Override
//            public void run(){
//                try{
//                    while(!isInterrupted()){
//                        Thread.sleep(1000);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                long date = System.currentTimeMillis();
//                                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss:a");
//                                time = sdf.format(date);
//                            }
//                        });
//
//                    }
//                }catch (InterruptedException e){
//
//                }
//            }
//
//        };
//        t.start();

        startExperiment = findViewById(R.id.start_experiment);
        startExperiment.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //We reset the PWD
        lengthOfCurrentPWD = 0;
        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.passFacePreferences), MODE_PRIVATE);

        loadParameters();
        updateGridView();

        listPhotosChosen = new ArrayList<>();
    }

    private void loadParameters() {

        numPhotosShown = sharedPreferences.getInt(getString(R.string.numPhotosShownPreference), Const.DEFAULT_PASSFACE_SIZE);
        //String typePhotos = sharedPreferences.getString(getString(R.string.typePhotosPreference), Const.DEFAULT_TYPE_PHOTOS);
        numPhotos = sharedPreferences.getInt(getString(R.string.numPhotosPreference), Const.DEFAULT_PASSWORD_LENGTH);
        numAttempts = sharedPreferences.getInt(getString(R.string.numberAttemptsPreference), Const.DEFAULT_NUM_ATTEMPTS);
        shufflePhotos = sharedPreferences.getBoolean(getString(R.string.shufflePhotosPreference), Const.DEFAULT_SHUFFLE);
        usePhotoMultipleTimes = sharedPreferences.getBoolean(getString(R.string.usePhotoMultipleTimesPreference), Const.DEFAULT_USE_PHOTO_MULTIPLE_TIMES);
        configId = sharedPreferences.getInt(getString(R.string.configIdPreference), 0);
        String savedPassword = sharedPreferences.getString(getString(R.string.passfacePasswordPreference), "");
        savedPasswordToArray = savedPassword.split("\\+");

        /*
        for (String s: savedPasswordToArray)
            savedPassfacePassword.add(s);
        */

        savedPassfacePassword.addAll(Arrays.asList(savedPasswordToArray));

        configIdTextView.setText(String.valueOf(configId));
    }

    private void updateGridView() {

        //list of names of photos to be displayed
        ArrayList<String> listPhotos = new ArrayList<>();
        //name of the correct photo that must be included in the list of photos to be displayed
        // the remaining photos are decoys
        String includePhoto = savedPasswordToArray[currentStep];
        //check the type of the photo to be included
        String includePhotoType = checkTypePhoto(includePhoto);
        listPhotos.add(includePhoto);

        int i = 1;
        switch (includePhotoType){
            case "Misc":
                while(listPhotos.size() < numPhotosShown){
                    if (!savedPassfacePassword.contains(Const.MISC[i]))
                        listPhotos.add(Const.MISC[i]);
                    i++;
                }
                break;
            case "Animals":
                while(listPhotos.size() < numPhotosShown){
                    if (!savedPassfacePassword.contains(Const.ANIMALS[i]))
                        listPhotos.add(Const.ANIMALS[i]);
                    i++;
                }
                break;
            case "Flowers":
                while(listPhotos.size() < numPhotosShown){
                    if (!savedPassfacePassword.contains(Const.FLOWERS[i]))
                        listPhotos.add(Const.FLOWERS[i]);
                    i++;
                }
                break;
            case "Nature":
                while(listPhotos.size() < numPhotosShown){
                    if (!savedPassfacePassword.contains(Const.NATURE[i]))
                        listPhotos.add(Const.NATURE[i]);
                    i++;
                }
                break;
            case "Fruit Trees":
                while(listPhotos.size() < numPhotosShown){
                    if (!savedPassfacePassword.contains(Const.TREES[i]))
                        listPhotos.add(Const.TREES[i]);
                    i++;
                }
                break;
        }

        //update the set of photos to be displayed
        String[] newArray = new String[listPhotos.size()];
        for (int j = 0; j < listPhotos.size(); j++)
            newArray[j] = listPhotos.get(j);

        gridView.setAdapter(new ImageAdapter(this, newArray, shufflePhotos));
    }


    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(PassFaceActivity.this, RunExperiment.class);
        finish();
        startActivity(intent);
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            case R.id.home_button:
                Intent intent = new Intent(PassFaceActivity.this, HomeUI.class);
                finish();
                startActivity(intent);
                break;
            case R.id.load_settings:
                Intent intent2 = new Intent(PassFaceActivity.this, PassfaceConfigsList.class);
                finish();
                startActivity(intent2);
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void removeBorders() {
        for (ImageView i : listPhotosChosen) {
            i.setPadding(0, 0, 0, 0);
            i.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.reset_password:
                reset();
                break;
            case R.id.start_experiment:
                showUserIdDialog();
                //showViews();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        ImageView i = (ImageView) ((LinearLayout) view).getChildAt(0);
        lengthOfCurrentPWD++;
        LogData logData;

        if(currentStep != savedPasswordToArray.length - 1){currentStep++;}

        if (lengthOfCurrentPWD <= numPhotos){

            String newPhotoName = i.getTag().toString();

            //Add the name of the selected photo to the password.
            // If user is allowed to use same photo multiple times
            if (usePhotoMultipleTimes){
                listPhotosChosen.add(i);
                i.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                i.setPadding(15, 15, 15, 15);
                enteredPassfacePassword.add(newPhotoName);

                //add data to the log data
                if(lengthOfCurrentPWD < numPhotos){
                    logData = new LogData(configIdTextView.getText().toString(),
                            configIdTextView.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.passface_user_clicks)+ i.getTag().toString(),
                            String.valueOf(currentNumAttempt),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                    logs.add(logData);
                }
                updateGridView();
            }else { // If user is not allowed to use same photo multiple times to make up his/her password
                if (!enteredPassfacePassword.contains(newPhotoName)){
                    listPhotosChosen.add(i);
                    i.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    i.setPadding(15, 15, 15, 15);
                    enteredPassfacePassword.add(newPhotoName);

                    //add data to the log data
                    if(lengthOfCurrentPWD < numPhotos){
                        logData = new LogData(configIdTextView.getText().toString(),
                                configIdTextView.getText().toString() + "." + String.valueOf(userId),
                                getString(R.string.passface_user_clicks)+ i.getTag().toString(),
                                String.valueOf(currentNumAttempt),
                                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                        logs.add(logData);
                    }
                    updateGridView();
                }else {
                    lengthOfCurrentPWD--;
                }
            }
        }

        if(lengthOfCurrentPWD == numPhotos){
            if (isEnteredPasswordCorrect()){
                currentStep = 0;
                enteredPassfacePassword.clear();
                lengthOfCurrentPWD = 0;
                updateGridView();
                removeBorders();

                logData = new LogData(configIdTextView.getText().toString(),
                        configIdTextView.getText().toString() + "." + String.valueOf(userId),
                        getString(R.string.passface_user_clicks) + i.getTag().toString(),
                        String.valueOf(currentNumAttempt),
                        TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                logs.add(logData);


                logData = new LogData(configIdTextView.getText().toString(),
                        configIdTextView.getText().toString() + "." + String.valueOf(userId),
                        getString(R.string.passface_selection_complete_correct),
                        String.valueOf(currentNumAttempt),
                        TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                logs.add(logData);

                userFinishExperiment();
                insertLastUserId(configId, userId);
                currentNumAttempt = 1;
                //hideViews();
                setViewsAfterExperiment();
                Alert.showCorrectPassfaceMsg(this);
            }else {
                if (currentNumAttempt == numAttempts){

                    Alert.maxAttemptsMsg(this);
                    //hideViews();
                    setViewsAfterExperiment();
                    currentStep = 0;
                    enteredPassfacePassword.clear();
                    lengthOfCurrentPWD = 0;
                    updateGridView();
                    removeBorders();

                    logData = new LogData(configIdTextView.getText().toString(),
                            configIdTextView.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.passface_user_clicks) + i.getTag().toString(),
                            String.valueOf(currentNumAttempt),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                    logs.add(logData);

                    logData = new LogData(configIdTextView.getText().toString(),
                            configIdTextView.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.passface_selection_complete_wrong),
                            String.valueOf(currentNumAttempt),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                    logs.add(logData);

                    userFinishExperiment();
                    insertLastUserId(configId, userId);

                    currentNumAttempt = 1;
                }else {
                    Alert.showWrongPassfaceMsg(this);
                    currentStep = 0;
                    enteredPassfacePassword.clear();
                    lengthOfCurrentPWD = 0;
                    updateGridView();
                    removeBorders();

                    logData = new LogData(configIdTextView.getText().toString(),
                            configIdTextView.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.passface_user_clicks) + i.getTag().toString(),
                            String.valueOf(currentNumAttempt),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                    logs.add(logData);

                    logData = new LogData(configIdTextView.getText().toString(),
                            configIdTextView.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.passface_selection_complete_wrong),
                            String.valueOf(currentNumAttempt),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                    logs.add(logData);
                    ++currentNumAttempt;
                }
            }
            try {
                LogData.writeLog(logs,getString(R.string.passfaces_log_file_name));
                logs.clear();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    /** A reset method that resets everything
     */
    private void reset(){

        LogData logData = new LogData(configIdTextView.getText().toString(),
                configIdTextView.getText().toString() + "." + String.valueOf(userId),
                "user clicks on reset button", String.valueOf(currentNumAttempt),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);

        removeBorders();
        currentStep = 0;
        enteredPassfacePassword.clear();
        lengthOfCurrentPWD = 0;
        updateGridView();
    }

    /** The method to check the type of the photos that make up the passface password
     * The photos were saved with prefixes to help in identifying their type.
     * Eg. all animal photos are prefixed with "anim" and all tree photos are prefixed
     * with "tree"
     * @param photoName: name of the photo to check its type
     * @return the type of the photo
     */
    private String checkTypePhoto(String photoName){
        String startsWith = photoName.substring(0,4);

        switch (startsWith){
            case "anim":
                return "Animals";
            case "natu":
                return "Nature";
            case "flow":
                return "Flowers";
            case "tree":
                return "Fruit Trees";
        }
        return "";
    }

    /** Method to verify if the saved set of passface photos is equal to the set of
     * entered photos by the user.
     */
    private boolean isEnteredPasswordCorrect(){
        for (int i = 0; i < savedPasswordToArray.length; i++){
            if (enteredPassfacePassword.get(i) != savedPassfacePassword.get(i))
                return false;
        }
        return true;
    }

    private void userStartsExperiment(){
        LogData  logData = new LogData(configIdTextView.getText().toString(),
                configIdTextView.getText().toString() + "." + String.valueOf(userId),
                getString(R.string.user_starts_experiment),
                String.valueOf(currentNumAttempt),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    private void userFinishExperiment(){
        LogData  logData = new LogData(configIdTextView.getText().toString(),
                configIdTextView.getText().toString() + "." + String.valueOf(userId),
                getString(R.string.user_finish_experiment), String.valueOf(currentNumAttempt),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    private void setViews4Experiment(){
        gridView.setVisibility(View.VISIBLE);
        startExperiment.setVisibility(View.INVISIBLE);
        configIdTextView.setVisibility(View.INVISIBLE);
        setupIdLabelTextview.setVisibility(View.INVISIBLE);
    }

    private void setViewsAfterExperiment(){
        gridView.setVisibility(View.INVISIBLE);
        startExperiment.setVisibility(View.VISIBLE);
        configIdTextView.setVisibility(View.VISIBLE);
        setupIdLabelTextview.setVisibility(View.VISIBLE);

    }


    /** This displays a dialog with the id of the user that
     * will perform the experiment for the experimenter each
     * time the experiment starts
     **/
    private void showUserIdDialog(){

        // Get the last used user Id and build up it
        try {
            userId = getLastUserId(configId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.layout_user_id_alert_dialog,null);
        alert.setView(dialogView);

        TextView userIdTextview = dialogView.findViewById(R.id.user_id_textview);
        userIdTextview.setText(configIdTextView.getText().toString() + "." + userId);
        alert.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setViews4Experiment();
                userStartsExperiment();
            }
        });
        alert.create().show();
    }

    private int getLastUserId(int configId) throws ExecutionException, InterruptedException {
        //getting the last user id from the app database
        PassfacesUserId passfacesUserId = appViewModel.getLastPassfacesUserId(configId);
        if (passfacesUserId != null){
            return passfacesUserId.getLastUserId() + 1;
        }
        // if this is the first user to do the experiment
        else {
            return 1;
        }
    }

    private void insertLastUserId(int configId, int userId){
        PassfacesUserId passfacesUserId = new PassfacesUserId(configId, userId);
        appViewModel.insertLastPassfacesUserId(passfacesUserId);
    }
}
