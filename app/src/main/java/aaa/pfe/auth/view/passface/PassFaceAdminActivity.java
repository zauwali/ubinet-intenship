package aaa.pfe.auth.view.passface;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import aaa.pfe.auth.CreateSettings;
import aaa.pfe.auth.R;
import aaa.pfe.auth.utils.Const;
import aaa.pfe.auth.view.mother.AdminActivity;

/** The passfaces setup creation activity
 * Updated by: Zauwali S. Paki
 * April 2018
 * */

public class PassFaceAdminActivity extends AdminActivity {

    //public static LogData logWriter;
    SharedPreferences sharedPreferences;

    ArrayAdapter<CharSequence> typePhotosAdapter;
    ArrayAdapter<CharSequence> numPhotosShownAdapter;

    private EditText numPhotosEditText;
    private EditText numAttemptsEditText;

    private Spinner typePhotosSpinner;
    private Spinner numPhotosShownSpinner;

    private int numPhotos;
    private int numPhotosShown;
    private int numAttempts;

    private String typePhotos;

    private SwitchCompat shufflePhotos, usePhotoMultipleTimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_face_admin);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        numPhotosEditText =  findViewById(R.id.editTextNumPhotos);
        numAttemptsEditText =  findViewById(R.id.editTextNumAttempts);

        typePhotosSpinner =  findViewById(R.id.spinnerTypePhotos);
        numPhotosShownSpinner = findViewById(R.id.spinner_num_photo_shown);

        shufflePhotos = findViewById(R.id.shuffle_photos);
        usePhotoMultipleTimes = findViewById(R.id.use_photo_multiple_times);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.passFacePreferences), MODE_PRIVATE);

        setSpinnerAdapters();
        loadSettings();

    }

    private void loadSettings() {

        String savedNumPhotos = String.valueOf(sharedPreferences.getInt(getString(R.string.numPhotosPreference), Const.DEFAULT_PASSFACE_SIZE));
        numPhotosEditText.setText(savedNumPhotos);

        String numAttempts = String.valueOf(sharedPreferences.getInt(getString(R.string.numberAttemptsPreference), Const.DEFAULT_NUM_ATTEMPTS));
        numAttemptsEditText.setText(numAttempts);

        boolean savedShuffle = sharedPreferences.getBoolean(getString(R.string.shufflePhotosPreference), Const.DEFAULT_SHUFFLE);
        shufflePhotos.setChecked(savedShuffle);


        boolean twicePhoto = sharedPreferences.getBoolean(getString(R.string.usePhotoMultipleTimesPreference), Const.DEFAULT_USE_PHOTO_MULTIPLE_TIMES);
        usePhotoMultipleTimes.setChecked(twicePhoto);

        typePhotosSpinner.setSelection(typePhotosAdapter.getPosition(sharedPreferences.getString(getString(R.string.typePhotosPreference), Const.DEFAULT_TYPE_PHOTOS)));

        numPhotosShown = Integer.parseInt(numPhotosShownSpinner.getSelectedItem().toString());
    }

    private void setSpinnerAdapters() {

        typePhotosAdapter = ArrayAdapter.createFromResource(this,
                R.array.typePhotos, android.R.layout.simple_spinner_item);

        typePhotosAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typePhotosSpinner.setAdapter(typePhotosAdapter);

        numPhotosShownAdapter = ArrayAdapter.createFromResource(this,
                R.array.num_photos_shown, android.R.layout.simple_spinner_item);

        numPhotosShownAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        numPhotosShownSpinner.setAdapter(numPhotosShownAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(PassFaceAdminActivity.this, CreateSettings.class);
        finish();
        startActivity(intent);
        return true;
    }

    //Save changes into the SharedPreferences
    @Override
    public void saveChanges() {

        setValues();
        if(parametersOk()){

            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putInt(getString(R.string.numPhotosShownPreference), numPhotosShown);
            editor.putInt(getString(R.string.numPhotosPreference), numPhotos);
            editor.putString(getString(R.string.typePhotosPreference), typePhotos);
            editor.putInt(getString(R.string.numberAttemptsPreference), numAttempts);
            editor.putBoolean(getString(R.string.shufflePhotosPreference), shufflePhotos.isChecked());
            editor.putBoolean(getString(R.string.usePhotoMultipleTimesPreference), usePhotoMultipleTimes.isChecked());

            editor.apply();

            AlertDialog.Builder alert = new AlertDialog.Builder(PassFaceAdminActivity.this);
            alert.setIcon(R.drawable.ic_info_teal);
            alert.setTitle("You are now going to choose the photos");
            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(PassFaceAdminActivity.this, SetPassfacePhotosActivity.class);
                    finish();
                    startActivity(i);
                }
            });
            alert.create().show();
        }
    }

    private void setRedColor(TextView v) {
        v.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
    }

    private boolean parametersOk() {
        if (numAttempts < 1) {
            TextView t1 =  findViewById(R.id.textViewnumAttempts);
            setRedColor(t1);
            Toast.makeText(getApplicationContext(), R.string.wrong_num_attempts_parameter, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (numPhotos < 1){
            Toast.makeText(this, R.string.wrong_num_photos_parameter, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void setValues(){

        numPhotosShown = Integer.parseInt(numPhotosShownSpinner.getSelectedItem().toString());
        numPhotos = Integer.parseInt(numPhotosEditText.getText().toString());
        typePhotos = typePhotosSpinner.getSelectedItem().toString();
        numAttempts = Integer.parseInt(numAttemptsEditText.getText().toString());

        numPhotos = Integer.valueOf(numPhotosEditText.getText().toString());
        typePhotos = typePhotosSpinner.getSelectedItem().toString();

    }


//    private ArrayList<String> buildSettingsLog() {
//        ArrayList<String> settings = new ArrayList<>();
//        String columns = "Number Of Photos;Type of Photos;Length per steps;Number of Steps;" +
//                "Number of Attempts;Type of Matching;Shuffle;Use twice an image;Capture mode";
//        settings.add(columns);
//
//        String paramVals = ";" + numPhotos + ";" + typePhotos + ";" + numPhotoPerStep + ";"
//                + numSteps + ";" + numAttempts + ";" + shufflePhotos + ";" + twicePhoto + ";";
//
//        settings.add(paramVals);
//        return settings;
//
//    }
//
//    private void createLog(ArrayList<String> settings) {
//        logWriter.writePassFaceParams(settings);
//    }

}


