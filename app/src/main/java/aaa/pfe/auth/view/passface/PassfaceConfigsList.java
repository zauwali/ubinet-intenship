package aaa.pfe.auth.view.passface;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.RunExperiment;
import database.AppViewModel;
import database.PassFaceConfigParams;

/** The activity responsible for the display of passfaces
 * setup list
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class PassfaceConfigsList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passface_configs_list);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        final PassfaceConfigsListAdapter adapter = new PassfaceConfigsListAdapter(PassfaceConfigsList.this);
        RecyclerView recyclerView = findViewById(R.id.image_list_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        appViewModel.getAllFacePassConfigParams().observe(this, new Observer<List<PassFaceConfigParams>>() {
            @Override
            public void onChanged(@Nullable List<PassFaceConfigParams> passFaceConfigParams) {
                adapter.setConfigurations(passFaceConfigParams);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home_button) {
            Intent i = new Intent(PassfaceConfigsList.this, HomeUI.class);
            finish();
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(PassfaceConfigsList.this, RunExperiment.class);
        finish();
        startActivity(intent);
        return true;
    }
}
