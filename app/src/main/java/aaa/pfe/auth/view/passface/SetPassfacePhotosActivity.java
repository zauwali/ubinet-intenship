package aaa.pfe.auth.view.passface;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.CreateSettings;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.utils.Const;
import database.AppViewModel;
import database.PassFaceConfigParams;
import de.hdodenhof.circleimageview.CircleImageView;

/** This activity is used to allow user to select the set of photos that will
 * make up the passface password.
 * Author: Zauwali S. Paki
 */

public class SetPassfacePhotosActivity extends AppCompatActivity implements
        View.OnClickListener, AdapterView.OnItemClickListener{


    private static final String TAG = "SetPassfacePhotosActivi";
    //View attributes
    private GridView gridView;

    //variables for controlling the photos to be displayed
    int startArray = 0, endArray;

    private TextView photoSelectionStepTextview;

    private CircleImageView showMorePhotos, donePhotoSelection, refreshPhotos;

    private SharedPreferences sharedPreferences;
    //Necessary for the PWD Treatment
    private ArrayList<String> passfacePassword = new ArrayList<>();
    private ArrayList<String> currentPassword = new ArrayList<>();
    private List<ImageView> listPhotosChosen;
    private ArrayList<String> photosToBeShown = new ArrayList<>();
    private ArrayList<String> selectedPhotos = new ArrayList<>();
    //to be used if a photo cannot be used twice to make up the passface password
    private int lengthOfCurrentPWD = 0;
    private int currentStep = 1;

    //Different parameters
    private int numPhotosShown;
    private String typePhotos;
    private int numPhotos;
    private int numAttempts;
    //private boolean orderOfPwd;
    private boolean shufflePhotos;
    private boolean usePhotoMultipleTimes;

    private AppViewModel appViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_passface_photos);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        photoSelectionStepTextview = findViewById(R.id.photo_selection_step_textview);

        showMorePhotos = findViewById(R.id.show_more_photos);
        donePhotoSelection = findViewById(R.id.done_photo_selection);
        refreshPhotos = findViewById(R.id.refresh_photos);

        appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.passFacePreferences), MODE_PRIVATE);
        loadParameters();
        loadPhotos(typePhotos);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //We reset the PWD
        lengthOfCurrentPWD = 0;
        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.passFacePreferences), MODE_PRIVATE);

        photoSelectionStepTextview = findViewById(R.id.photo_selection_step_textview);
        loadParameters();
        loadPhotos(typePhotos);

        setSeletionStepCaption();

        gridView = findViewById(R.id.gridView);
        updateGridView();

        listPhotosChosen = new ArrayList<>();

        setupListeners();
    }

    private void loadParameters() {

        numPhotosShown = sharedPreferences.getInt(getString(R.string.numPhotosShownPreference), Const.DEFAULT_PASSFACE_SIZE);
        numPhotos = sharedPreferences.getInt(getString(R.string.numPhotosPreference), Const.DEFAULT_NUM_PHOTOS);
        typePhotos = sharedPreferences.getString(getString(R.string.typePhotosPreference), Const.DEFAULT_TYPE_PHOTOS);
        shufflePhotos = sharedPreferences.getBoolean(getString(R.string.shufflePhotosPreference), Const.DEFAULT_SHUFFLE);
        usePhotoMultipleTimes = sharedPreferences.getBoolean(getString(R.string.usePhotoMultipleTimesPreference), Const.DEFAULT_USE_PHOTO_MULTIPLE_TIMES);
        numAttempts = sharedPreferences.getInt(getString(R.string.numberAttemptsPreference), Const.DEFAULT_NUM_ATTEMPTS);
        endArray = numPhotosShown;
    }

    //Method to set up all the click listeners of our view
    private void setupListeners() {
        gridView.setOnItemClickListener(this);
        showMorePhotos.setOnClickListener(this);
        donePhotoSelection.setOnClickListener(this);
        refreshPhotos.setOnClickListener(this);
    }

    private void updateGridView() {
        String[] newArray = new String[selectedPhotos.size()];

        for (int i = 0; i < selectedPhotos.size(); i++)
            newArray[i] = selectedPhotos.get(i);

        gridView.setAdapter(new ImageAdapter(this, newArray, shufflePhotos));

    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(SetPassfacePhotosActivity.this, CreateSettings.class);
        finish();
        startActivity(intent);
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            case R.id.home_button:
                Intent intent = new Intent(SetPassfacePhotosActivity.this, HomeUI.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void removeBorders() {
        for (ImageView i : listPhotosChosen) {
            i.setPadding(0, 0, 0, 0);
            i.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch(id){
            case R.id.done_photo_selection:
                savePassfaceParams();
                break;
            case R.id.refresh_photos:
                resetPhotoSelection();
                break;
            case R.id.show_more_photos:
                loadMorePhotos();
                break;
        }
    }

    //handles image click on the gridview
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        lengthOfCurrentPWD++;


        if (lengthOfCurrentPWD <= numPhotos){

            ImageView i = (ImageView) ((LinearLayout) view).getChildAt(0);
            String newPhotoName = i.getTag().toString();

            //Add the name of the selected photo to the password.
            // If user is allowed to use same photo multiple times
            if (usePhotoMultipleTimes){
                listPhotosChosen.add(i);
                i.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                i.setPadding(15, 15, 15, 15);
                passfacePassword.add(newPhotoName);
                selectNextPhoto();
                if (lengthOfCurrentPWD == numPhotos){
                    donePhotoSelection.setVisibility(View.VISIBLE);
                }else setSeletionStepCaption();
            }else { // If user is not allowed to use same photo multiple times to make up his/her password
                if (!passfacePassword.contains(newPhotoName)){
                    listPhotosChosen.add(i);
                    i.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    i.setPadding(15, 15, 15, 15);
                    passfacePassword.add(newPhotoName);
                    selectNextPhoto();
                    if (lengthOfCurrentPWD == numPhotos){
                        donePhotoSelection.setVisibility(View.VISIBLE);
                    }else setSeletionStepCaption();
                }else {
                    lengthOfCurrentPWD--;
                }
            }
        }
    }

    //handles the select of next set of photos for the passface
    private void selectNextPhoto(){
        currentStep++;
        removeBorders();
        updateGridView();

    }

    //saves the set of photos and other parameters for a given configuration
    private void savePassfaceParams(){
        if (!currentPassword.isEmpty()){
            passfacePassword.addAll(currentPassword);
            currentPassword.clear();
        }

        PassFaceConfigParams params = new PassFaceConfigParams(numPhotosShown, numPhotos,
                typePhotos,shufflePhotos,usePhotoMultipleTimes,passfacePassword,numAttempts);

        appViewModel.insertFacePassConfigParams(params);

        Activity[] activities = new Activity[]{new PassFaceAdminActivity(), new CreateSettings()};
        Alert.configSavedMsg(SetPassfacePhotosActivity.this, activities);

    }

    /** Method that displays the title that indicates the step in
     * the selection of set of photos for the pass face password
     */
    private void setSeletionStepCaption(){
        photoSelectionStepTextview.setText(getResources().getString(R.string.photo_selection_step)
                + " " + currentStep + getResources().getString(R.string.of) + " " + String.valueOf(numPhotos));
    }

    /** The method that resets the selected images in the current step
     */
    private void resetPhotoSelection(){

        removeBorders();
        // Reset
        currentStep = 1;
        donePhotoSelection.setVisibility(View.INVISIBLE);
        passfacePassword.clear();
        lengthOfCurrentPWD = 0;
        startArray = 0;
        endArray = numPhotosShown;
        updateGridView();
        setSeletionStepCaption();

    }

/** Method to load more photos for the user to select.
 * The counter variable is used to know by how much to increase
 * the index of the list that contains lists of photos from which
 * we take.
 */
    private void loadMorePhotos(){

        //clear the previously selected photos in order to select new photos
        selectedPhotos.clear();

        int counter = 0;
        if (endArray < photosToBeShown.size()){
            while (counter < numPhotosShown){
                if (endArray == photosToBeShown.size())
                    break;
                startArray++;
                endArray++;
                counter++;
            }
        }
        if (endArray == photosToBeShown.size()){
            startArray = 0;
            endArray = numPhotosShown;
        }

        for (int i = startArray; i < endArray; i++)
            selectedPhotos.add(photosToBeShown.get(i));

        updateGridView();

    }

    /** The method that loads the needed photos to be displayed for
     * the user to select
     * @param typePhotos: the type of photos to be loaded
     */

    private void loadPhotos(String typePhotos){

        photosToBeShown.clear();
        selectedPhotos.clear();

        //load the photos from the pool
        switch (typePhotos){
            case "Misc":
                for (int i = 0; i < Const.MISC.length; i++)
                    photosToBeShown.add(Const.MISC[i]);
                break;
            case "Animals":
                for (int i = 0; i < Const.ANIMALS.length; i++)
                    photosToBeShown.add(Const.ANIMALS[i]);
                break;
            case "Fruit Trees":
                for (int i = 0; i < Const.TREES.length; i++)
                    photosToBeShown.add(Const.TREES[i]);
                break;
            case "Nature":
                for (int i = 0; i < Const.NATURE.length; i++)
                    photosToBeShown.add(Const.NATURE[i]);
                break;
            case "Flowers":
                for (int i = 0; i < Const.FLOWERS.length; i++)
                    photosToBeShown.add(Const.FLOWERS[i]);
                break;
        }

        //adding the photos the list of selected photos equal to
        //the number of photos as specified during setup creation
        for (int j = 0; j < numPhotosShown; j++)
            selectedPhotos.add(photosToBeShown.get(j));

    }

}
