package aaa.pfe.auth.view.pincode;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.DeleteSetups;
import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PincodeConfigParams;

/** This is class is used to delete a specific Pin Code configuration
 * Author: Zauwali S. Paki
 */
public class DeletePincodeConfigs extends AppCompatActivity {

    private List<PincodeConfigParams> pincodeSetups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_pincode_param);


        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final DeletePincodeConfigsAdapter adapter = new DeletePincodeConfigsAdapter(this);
        final RecyclerView recyclerView = findViewById(R.id.delete_pincode_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final TextView titleTextView = findViewById(R.id.title_textview);
        final TextView empty_view = findViewById(R.id.empty_view);

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);
        try {
            appViewModel.getAllPincodeSettings().observe(this, new Observer<List<PincodeConfigParams>>() {
                @Override
                public void onChanged(@Nullable List<PincodeConfigParams> pincodeConfigParams) {

                    adapter.setConfigurations(pincodeConfigParams);

                    pincodeSetups = pincodeConfigParams;
                    if (pincodeSetups.size()  == 0){
                        titleTextView.setVisibility(View.GONE);
                        empty_view.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }else {
                        titleTextView.setVisibility(View.VISIBLE);
                        empty_view.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(DeletePincodeConfigs.this, DeleteSetups.class);
        finish();
        startActivity(intent);
        return true;
    }
}
