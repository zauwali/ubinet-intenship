package aaa.pfe.auth.view.pincode;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.RunExperiment;
import aaa.pfe.auth.utils.LogData;
import aaa.pfe.auth.utils.Pair;
import aaa.pfe.auth.utils.TimeAndDate;
import aaa.pfe.auth.view.pincodeview.IndicatorDots;
import aaa.pfe.auth.view.pincodeview.PinLockListener;
import aaa.pfe.auth.view.pincodeview.PinLockView;
import database.AppViewModel;
import database.PincodUserId;
import de.hdodenhof.circleimageview.CircleImageView;

/** The activity for doing PIN code experiment
 * Updated by: Zauwali S. Paki
 * April 2018
 * */


public class PinCodeActivity extends AppCompatActivity {


    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;

    private int  indicatorType;
    SharedPreferences sharedPreferences;

    ArrayList<LogData> logs = new ArrayList<>();
    private boolean shuffle;
    private int userId, configId;

    private int numTrial = 1, maxTrials;
    long lastTouchTime;
    long begin;
    Map<Integer,Pair<String,Long>> toucheMap;

    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {

            LogData logData;

            if (sharedPreferences.getString("pinCode", null).equals(pin)) {
                //Correct PIN

                //Add user action to the log
                logData = new LogData(configIdTextview.getText().toString(),
                        configIdTextview.getText().toString() + "." + String.valueOf(userId),
                        getString(R.string.pincode_user_taps_on) + pin.charAt(pin.length() - 1),
                        String.valueOf(numTrial),
                        TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                logs.add(logData);

                logData = new LogData(configIdTextview.getText().toString(),
                        configIdTextview.getText().toString() + "." + String.valueOf(userId),
                        getString(R.string.pincode_complete_correct), String.valueOf(numTrial),
                        TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                logs.add(logData);
                userFinishExperiment();
                //hideViews();
                setViewsAfterExperiment();

                try {
                    LogData.writeLog(logs, getString(R.string.pincode_log_file_name));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                logs.clear();

                //to keep the last used user Id in this configuration
                insertLastUserId(configId, userId);

                Alert.showCorrectPinMsg(PinCodeActivity.this);
                saveTouch(pin.length(),pin);//last touch
                numTrial = 1;

            } else {//Incorrect PIN
                if (numTrial == maxTrials){

                    logData = new LogData(configIdTextview.getText().toString(),
                            configIdTextview.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.pincode_user_taps_on) + pin.charAt(pin.length() - 1),
                            String.valueOf(numTrial),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                    logs.add(logData);

                    logData = new LogData(configIdTextview.getText().toString(),
                            configIdTextview.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.pincode_complete_wrong), String.valueOf(numTrial),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                    logs.add(logData);
                    userFinishExperiment();

                    try {
                        LogData.writeLog(logs, getString(R.string.pincode_log_file_name));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    logs.clear();

                    //to keep the last used user Id in this configuration
                    insertLastUserId(configId, userId);

                    //hideViews();
                    setViewsAfterExperiment();
                    Alert.maxAttemptsMsg(PinCodeActivity.this);
                    numTrial = 1;

                }else {
                    logData = new LogData(configIdTextview.getText().toString(),
                            configIdTextview.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.pincode_user_taps_on) + pin.charAt(pin.length() - 1),
                            String.valueOf(numTrial),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                    logs.add(logData);

                    logData = new LogData(configIdTextview.getText().toString(),
                            configIdTextview.getText().toString() + "." + String.valueOf(userId),
                            getString(R.string.pincode_complete_wrong), String.valueOf(numTrial),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                    logs.add(logData);

                    try {
                        LogData.writeLog(logs, getString(R.string.pincode_log_file_name));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    logs.clear();
                    Alert.showWrongPinMsg(PinCodeActivity.this);
                    numTrial++;

                }

            }

            if (shuffle)
                mPinLockView.enableLayoutShuffling();
            mPinLockView.resetPinLockView();
        }

        @Override
        public void onEmpty() {
            mPinLockView.resetPinLockView();
        }

        @Override
        public void onPinChange(int intermediatePinLength, String intermediatePin) {

            saveTouch(intermediatePinLength,intermediatePin);
            LogData logData = new LogData(configIdTextview.getText().toString(),
                    configIdTextview.getText().toString() + "." + String.valueOf(userId),
                    getString(R.string.pincode_user_taps_on) + intermediatePin.charAt(intermediatePin.length()-1),
                    String.valueOf(numTrial),
                    TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
            logs.add(logData);
        }

    };

    private void saveTouch(int length, String intermediatePin) {
        long currentTime = System.currentTimeMillis();
        lastTouchTime = currentTime - begin;
        toucheMap.put(length,new Pair<String, Long>(intermediatePin.substring(intermediatePin.length()-1, intermediatePin.length()),lastTouchTime));
    }

    private TextView configIdTextview, pinCodeTextview, pincodHeaderTextview;
    private TextView pincodeTextviewLabel, setupIdLabel;
    private CircleImageView startExperiment;

    private AppViewModel appViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_code);


        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.pincode_auth_windonw_title);

        appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        configIdTextview =  findViewById(R.id.setup_id_textview);
        pinCodeTextview = findViewById(R.id.pin_code);
        pincodHeaderTextview = findViewById(R.id.pincod_header_textview);
        pincodeTextviewLabel = findViewById(R.id.pincode_textview_label);
        setupIdLabel = findViewById(R.id.setup_id_label);

        sharedPreferences = getSharedPreferences(getString(R.string.pincode_shared_preferences), Context.MODE_PRIVATE);

        //PINLOCKVIEW
        mPinLockView = findViewById(R.id.pin_lock_view);
        mIndicatorDots = findViewById(R.id.indicator_dots);

        startExperiment = findViewById(R.id.start_experiment);
        startExperiment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showViews();
                //userStartsExperiment();
                showUserIdDialog();
            }
        });

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);

        mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.teal_dark));
        mPinLockView.setTextSize(80);
        mPinLockView.setDeleteButtonSize(80);

        /**
        userIdSpinner = findViewById(R.id.users_id_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.users_id, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userIdSpinner.setAdapter(adapter);
         */

    }

    @Override
    protected void onResume() {
        super.onResume();
        //re-initialize Pin
        mPinLockView.resetPinLockView();
        numTrial = 1;

        /*PARAMS*/
        int pinLength =sharedPreferences.getInt("pincodeLength",4);
        mPinLockView.setPinLength(pinLength);


        maxTrials = sharedPreferences.getInt("maxTrials",3);

        if (sharedPreferences.contains("shufflePad")&&sharedPreferences.getBoolean("shufflePad",false)){
            mPinLockView.enableLayoutShuffling();
            shuffle = true;
        }else {
            mPinLockView.setCustomKeySet(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0});
            shuffle = false;
        }

        indicatorType=sharedPreferences.getInt("indicators",-1);
        if (indicatorType>=0){
            if (!mPinLockView.isIndicatorDotsAttached()) {
                mIndicatorDots.setVisibility(View.VISIBLE);
                mPinLockView.attachIndicatorDots(mIndicatorDots);
            }
            mIndicatorDots.setIndicatorType(indicatorType);

        }else{
            mPinLockView.takeOffIndicatorDots();
            mIndicatorDots.setVisibility(View.INVISIBLE);
        }

        configId = sharedPreferences.getInt("configId",0);
        configIdTextview.setText(String.valueOf(configId));

        String thePin = sharedPreferences.getString("pinCode","");
        pinCodeTextview.setText(thePin);

        toucheMap = new HashMap();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(PinCodeActivity.this, RunExperiment.class);
        finish();
        startActivity(intent);
        return true;
    }


    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.load_settings) {
            Intent i = new Intent(PinCodeActivity.this, PincodeConfigParamsList.class);
            finish();
            startActivity(i);
        }
        if (id == R.id.home_button) {
            Intent i = new Intent(PinCodeActivity.this, HomeUI.class);
            finish();
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void userStartsExperiment(){

        LogData  logData = new LogData(configIdTextview.getText().toString(),
                configIdTextview.getText().toString() + "." + String.valueOf(userId),
                getString(R.string.user_starts_experiment),
                String.valueOf(numTrial),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    private void userFinishExperiment(){
        LogData  logData = new LogData(configIdTextview.getText().toString(),
                configIdTextview.getText().toString() + "." + String.valueOf(userId),
                getString(R.string.user_finish_experiment),
                String.valueOf(numTrial),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    private void showViews(){
        startExperiment.setVisibility(View.INVISIBLE);
        mPinLockView.setVisibility(View.VISIBLE);
        if (indicatorType >= 0){
            mIndicatorDots.setVisibility(View.VISIBLE);
        }
        pincodHeaderTextview.setVisibility(View.VISIBLE);
    }

    private void hideViews(){
        startExperiment.setVisibility(View.VISIBLE);
        mPinLockView.setVisibility(View.INVISIBLE);
        mIndicatorDots.setVisibility(View.INVISIBLE);
        pincodHeaderTextview.setVisibility(View.INVISIBLE);
        configIdTextview.setVisibility(View.INVISIBLE);
        pinCodeTextview.setVisibility(View.INVISIBLE);
    }

    private void setViews4Experiment(){
        startExperiment.setVisibility(View.INVISIBLE);
        configIdTextview.setVisibility(View.INVISIBLE);
        pinCodeTextview.setVisibility(View.INVISIBLE);
        pincodeTextviewLabel.setVisibility(View.INVISIBLE);
        setupIdLabel.setVisibility(View.INVISIBLE);

        mPinLockView.setVisibility(View.VISIBLE);
        if (indicatorType >= 0){
            mIndicatorDots.setVisibility(View.VISIBLE);
        }
        pincodHeaderTextview.setVisibility(View.VISIBLE);
    }

    private void setViewsAfterExperiment(){
        startExperiment.setVisibility(View.VISIBLE);
        configIdTextview.setVisibility(View.VISIBLE);
        pinCodeTextview.setVisibility(View.VISIBLE);
        pincodeTextviewLabel.setVisibility(View.VISIBLE);
        setupIdLabel.setVisibility(View.VISIBLE);

        mPinLockView.setVisibility(View.INVISIBLE);
        mIndicatorDots.setVisibility(View.INVISIBLE);
        pincodHeaderTextview.setVisibility(View.INVISIBLE);

    }

    private void userClicksDeleteButton(){

        LogData  logData = new LogData(configIdTextview.getText().toString(),
                configIdTextview.getText().toString() + "." + String.valueOf(userId),
                getString(R.string.user_clicks_delete_button),
                String.valueOf(numTrial),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    /** This displays a dialog with the id of the user that
     * will perform the experiment for the experimenter each
     * time the experiment starts
     **/
    private void showUserIdDialog(){

        // Get the last used user Id and build up it
        try {
            userId = getLastUserId(configId);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.layout_user_id_alert_dialog, null);
        alert.setView(dialogView);

        TextView userIdTextview = dialogView.findViewById(R.id.user_id_textview);
        userIdTextview.setText(configIdTextview.getText().toString() + "." + userId);
        alert.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setViews4Experiment();
                userStartsExperiment();
            }
        });
        alert.create().show();
    }

    private int getLastUserId(int configId) throws ExecutionException, InterruptedException {
        //getting the last user id from the app database
        PincodUserId pincodUserId = appViewModel.getLastUserId(configId);
        if (pincodUserId != null){
            return pincodUserId.getLastUserId() + 1;
        }
        // if this is the first user to do the experiment
        else {
            return 1;
        }
    }

    private void insertLastUserId(int configId, int userId){
        PincodUserId pincodUserId = new PincodUserId(configId, userId);
        appViewModel.insertLastUserId(pincodUserId);
    }
}
