package aaa.pfe.auth.view.pincode;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.CreateSettings;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.view.mother.AdminActivity;
import database.AppViewModel;
import database.PincodeConfigParams;

/** The activity for the creation of PIN code configuration setup
 * Updated by: Zauwali S. Paki
 * April 2018
 * */

public class PinCodeAdminActivity extends AdminActivity {

    //for saving the a configuration setting to the database
    private AppViewModel appViewModel;

    private ToggleButton shufflePad;

    private EditText pinCode;

    private RadioGroup radiogroupIndicators;
    private RadioButton rBNoDots, rBShowDotsToFill, rBAppearindDots, rBShowLastNum;

    SharedPreferences sharedPreferences;

    private Spinner pincondeLength, numTrials;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_code_admin);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.pincode_settings_window_title);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pinCode.getText().toString().isEmpty()){
                    Toast.makeText(PinCodeAdminActivity.this,
                            "Specify the Pin Code Pls", Toast.LENGTH_LONG).show();
                    return;
                }
                saveChanges();
            }
        });
        // for accessing the app database
        appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        //Retrieve Button
        shufflePad =  findViewById(R.id.toggleRandomNum);
        radiogroupIndicators = findViewById(R.id.radioGroupIndicator);
        rBNoDots = findViewById(R.id.radioButton1);
        rBShowDotsToFill = findViewById(R.id.radioButton2);
        rBAppearindDots = findViewById(R.id.radioButton3);
        rBShowLastNum = findViewById(R.id.radioButton4);

        pinCode = findViewById(R.id.pinCode);

        //Pin Code Length spinner
        pincondeLength = findViewById(R.id.pincodeLenghtSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.pinLength_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pincondeLength.setAdapter(adapter);
        pincondeLength.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pinCode.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
                        Integer.parseInt(pincondeLength.getSelectedItem().toString())) });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        numTrials = findViewById(R.id.numTrialsSpinner);
        ArrayAdapter<CharSequence> maxTryAdapter = ArrayAdapter.createFromResource(this,
                R.array.maxTrials, android.R.layout.simple_spinner_item);
        maxTryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        numTrials.setAdapter(maxTryAdapter);


        //Shared Pref
        sharedPreferences = getSharedPreferences(getString(R.string.pincode_shared_preferences), Context.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDefaultsValues();
        shufflePad.forceLayout();
    }

    private void setDefaultsValues() {

        if (sharedPreferences.contains("shufflePad")){
            shufflePad.setChecked(sharedPreferences.getBoolean("shufflePad",true));
        }

        if (sharedPreferences.contains("indicators")){
            int indicatorType=  sharedPreferences.getInt("indicators",-1);
            switch (indicatorType){
                case -1:
                    rBNoDots.setChecked(true);
                    break;
                case 0:
                    rBShowDotsToFill.setChecked(true);
                    break;
                case 2:
                    rBAppearindDots.setChecked(true);
                    break;
                case 3:
                    rBShowLastNum.setChecked(true);
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(PinCodeAdminActivity.this, CreateSettings.class);
        finish();
        startActivity(intent);
        return true;
    }

    @Override
    public void retrieveChanges(View v) {
        Toast t = Toast.makeText(this, "Save Parameters", Toast.LENGTH_SHORT);
        saveChanges();
        t.show();
    }

    @Override
    public void saveChanges() {
        Log.i("PinCodeAdmin","saveChanges");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("shufflePad", shufflePad.isChecked());
        editor.putInt("pinCodeLength",Integer.valueOf(pincondeLength.getSelectedItem().toString()));
        editor.putInt("numTrials",Integer.valueOf(numTrials.getSelectedItem().toString()));
        editor.putString("pinCode", pinCode.getText().toString());

        int selectedId = radiogroupIndicators.getCheckedRadioButtonId();
        if(selectedId == rBNoDots.getId()) {
            editor.putInt("indicators",-1);
        } else if(selectedId == rBShowDotsToFill.getId()) {
            editor.putInt("indicators",0);

        } else if (selectedId == rBAppearindDots.getId()){
            editor.putInt("indicators",2); //ou 1
        }else if (selectedId == rBShowLastNum.getId()){
            editor.putInt("indicators",3);
        }

        editor.apply();

        //saving the setting to the database
        int pincodeLength = Integer.parseInt(pincondeLength.getSelectedItem().toString());
        String thePincode = pinCode.getText().toString();
        int numRetries = Integer.parseInt(numTrials.getSelectedItem().toString());
        boolean isRandomPad = shufflePad.isChecked();
        boolean isNoIndicator = rBNoDots.isChecked();
        boolean isShowLastNum = rBShowLastNum.isChecked();
        boolean isAppearingDots = rBAppearindDots.isChecked();
        boolean isDotsToFill = rBShowDotsToFill.isChecked();


        PincodeConfigParams mPincodeConfigParams = new PincodeConfigParams(pincodeLength,thePincode,numRetries,isRandomPad,isNoIndicator,
                isShowLastNum,isDotsToFill,isAppearingDots);

        appViewModel.insertPincodeSetting(mPincodeConfigParams);

        Activity[] activities = new Activity[]{new PinCodeAdminActivity(), new CreateSettings()};
        Alert.configSavedMsg(PinCodeAdminActivity.this, activities);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home_button) {
            Intent i = new Intent(PinCodeAdminActivity.this, HomeUI.class);
            finish();
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
