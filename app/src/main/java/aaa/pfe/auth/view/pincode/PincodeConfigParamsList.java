package aaa.pfe.auth.view.pincode;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.RunExperiment;
import aaa.pfe.auth.view.schemepattern.CreatePattern;
import database.AppViewModel;
import database.PincodeConfigParams;

/** The activity that displays the list of PIN code setups
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class PincodeConfigParamsList extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pincode_config_params_list);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        final PincodeConfigParamsListAdapter adapter = new PincodeConfigParamsListAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.config_list_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // This observes changes in the set of pin code configurations automatically
        try {
            appViewModel.getAllPincodeSettings().observe(this, new Observer<List<PincodeConfigParams>>() {
                @Override
                public void onChanged(@Nullable List<PincodeConfigParams> pincodeConfigParams) {
                    adapter.setConfigurations(pincodeConfigParams);
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home_button) {

            Intent i = new Intent(PincodeConfigParamsList.this, HomeUI.class);
            finish();
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(PincodeConfigParamsList.this, RunExperiment.class);
        finish();
        startActivity(intent);
        return true;
    }
}
