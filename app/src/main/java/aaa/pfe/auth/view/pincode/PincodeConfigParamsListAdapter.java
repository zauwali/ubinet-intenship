package aaa.pfe.auth.view.pincode;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PincodeConfigParams;

/** Adapter class for PincodeConfigParamsList
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class PincodeConfigParamsListAdapter extends RecyclerView.Adapter<PincodeConfigParamsListAdapter.ViewHolder>{

    private List<PincodeConfigParams> mPincodeConfigParams;
    private Context context;
    private Application application;
    private AppViewModel appViewModel;

    //for keeping the default/current pin code configuration
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public PincodeConfigParamsListAdapter(Context context) {

        this.context = context;
        application = new Application();
        appViewModel = new AppViewModel(application);
        preferences = context.getSharedPreferences(context.getString(R.string.pincode_shared_preferences), context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_pincode_configs_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final PincodeConfigParams configParams = mPincodeConfigParams.get(position);
        final StringBuilder builder = new StringBuilder();

        String randomPad = configParams.isShufflePad()? context.getString(R.string.on) : context.getString(R.string.off);
        String dotsToFill = configParams.isDotsToFill()? context.getString(R.string.on) : context.getString(R.string.off);
        String showLastNum = configParams.isShowLastNum()? context.getString(R.string.on) : context.getString(R.string.off);
        String appearingDots = configParams.isAppearingDots()? context.getString(R.string.on) : context.getString(R.string.off);
        final String isNoIndicator = configParams.isNoIndicator()? context.getString(R.string.on) : context.getString(R.string.off);
        String pinCode = configParams.getPinCode();
        final int pincodeLength = configParams.getPincodeLength();
        final int numRetries = configParams.getMaxTrials();
        final int configId = configParams.getConfigId();

        builder.append("Configuration Id: " + configId + "\n");
        builder.append("Pin Code Length: " + pincodeLength + "\n");
        builder.append("Pin Code: " + pinCode + "\n");
        builder.append("Number of Tries: " + numRetries + "\n");
        builder.append("Enable Random Pad: " + randomPad + "\n");
        builder.append("No Indicator: " + isNoIndicator + "\n");
        builder.append("Show Dots to Fill: " + dotsToFill + "\n");
        builder.append("Show Last Number Entered: " + showLastNum + "\n");
        builder.append("Show Appearing Dots: " + appearingDots);

        // Display the setting in a textview
        holder.pincodeParamsTextview.setText(builder);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor = preferences.edit();
                editor.putInt("configId", configParams.getConfigId());
                editor.putBoolean("shufflePad",configParams.isShufflePad());
                editor.putInt("pincodeLength",configParams.getPincodeLength());
                editor.putString("pinCode", configParams.getPinCode());
                editor.putInt("maxTrials",configParams.getMaxTrials());
                editor.putBoolean("captureMode",true);

                if (configParams.isNoIndicator()){
                    editor.putInt("indicators",-1);
                }
                if (configParams.isDotsToFill()){
                    editor.putInt("indicators", 0);
                }
                if (configParams.isAppearingDots()){
                    editor.putInt("indicators", 2);
                }
                if (configParams.isShowLastNum()){
                    editor.putInt("indicators", 3);
                }

                editor.apply();
                Intent intent = new Intent(context, PinCodeActivity.class);
                ((Activity)context).finish();
                context.startActivity(intent);
            }
        });
    }

    void setConfigurations(List<PincodeConfigParams> configurations){
        mPincodeConfigParams = configurations;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mPincodeConfigParams == null){
            return 0;
        }else {
            return mPincodeConfigParams.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView pincodeParamsTextview;
        private LinearLayout linearLayout;
        ViewHolder(View itemView) {
            super(itemView);
            pincodeParamsTextview = itemView.findViewById(R.id.pincode_params_textview);
            linearLayout = itemView.findViewById(R.id.layout_pincode_configs_content);

        }
    }
}
