package aaa.pfe.auth.view.schemepattern;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.CreateSettings;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.view.patternlockview.PatternLockView;
import aaa.pfe.auth.view.patternlockview.listener.PatternLockViewListener;
import aaa.pfe.auth.view.patternlockview.utils.PatternLockUtils;
import database.AppViewModel;
import database.PatternConfigParams;

/** The activity for the creation of a pattern
 * Author: Zauwali S. Paki
 * April 2018
 */

public class CreatePattern extends AppCompatActivity implements View.OnClickListener{
    private PatternLockView mPatternLockView;
    SharedPreferences patternSharedPreferences;

    private int numRows = 3;
    private int numCols = 3;
    private int patternLength;
    private int dotSize = 30;
    private boolean vibration;
    private  Bitmap patternImage;
    private String patternString;

    Button takeScreenCapture, savePatternSetting;
    ImageView captureScreen;

    //private ArrayList<String> logsArray = new ArrayList<>();


    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {

        //private long begin;
        @Override
        public void onStarted() {
            //Log.d(getClass().getName(), "Pattern drawing started");
            //begin = System.currentTimeMillis();
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {

               // String dot = PatternLockUtils.getLastClickedDot(mPatternLockView, progressPattern);
                //Log.d(getClass().getName(), "Pattern progress:" + dot);
                //Log.d(getClass().getName(), "Pattern Size:" + progressPattern.size());
                //logsArray.add(dot);
                //long currentTime = System.currentTimeMillis();
                //long lastTouchTime = currentTime - begin;
                //logsArray.add(lastTouchTime + "");
                //Log.d("test","test");

        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern, boolean sizeReached) {
            //Log.d(getClass().getName(), "Pattern complete: " +
            //        PatternLockUtils.patternToString(mPatternLockView, pattern));

            patternString = PatternLockUtils.patternToSha1(mPatternLockView, pattern);
            takeScreenCapture.setEnabled(true);

            //Log.d("Save pattern sha1",patternString);

            if(sizeReached){
                //Log.d(getClass().getName(), "Max Size Reached: " +
                //        PatternLockUtils.patternToString(mPatternLockView, pattern));
                Toast.makeText(CreatePattern.this, "Max Size Reached", Toast.LENGTH_SHORT).show();

                //to disallow user from
                mPatternLockView.setEnabled(false);
            }
        }
        @Override
        public void onCleared() {
            //Log.d(getClass().getName(), "Pattern has been cleared");
        }

        @Override
        public void onSizeReached(List<PatternLockView.Dot> pattern) {

        }
    };

    private AppViewModel appViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pattern_user);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        patternSharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.pattern_lock_shared_preference), Context.MODE_PRIVATE);

        //editor = patternSharedPreferences.edit();

        mPatternLockView =  findViewById(R.id.pattern_lock_view);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);

        savePatternSetting = findViewById(R.id.save_pattern_setting);
        savePatternSetting.setOnClickListener(this);

        captureScreen = findViewById(R.id.captured_pattern);
        takeScreenCapture = findViewById(R.id.take_screen_shot);
        takeScreenCapture.setOnClickListener(this);
        setAllPreferences();
    }

    private void setAllPreferences() {

        if(patternSharedPreferences.contains("numRows")){
            numRows = patternSharedPreferences.getInt("numRows", 3);
        }else numRows = 3;

        if(patternSharedPreferences.contains("numCols")){
            numCols = patternSharedPreferences.getInt("numCols", 3);
        }else numCols = 3;

        if(patternSharedPreferences.contains("patternLength")){
            patternLength = patternSharedPreferences.getInt("patternLength", 3);
        }else patternLength = numCols * numCols;

        if(patternSharedPreferences.contains("dotSize")){
            dotSize = patternSharedPreferences.getInt("dotSize", 30);
        }else dotSize = 30;

        vibration = patternSharedPreferences.contains("vibration")
                && patternSharedPreferences.getBoolean("vibration",false);

        mPatternLockView.setPatternMaxSize(patternLength + 1);

        mPatternLockView.setTactileFeedbackEnabled(vibration);

        mPatternLockView.setInStealthMode(false);

        mPatternLockView.setDotNormalSize(dotSize);

        mPatternLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_FREE);

        mPatternLockView.setDotCount(numCols, numRows);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.take_screen_shot:
                patternImage = TakePatternScreenShot.takeScreenShotOfView(mPatternLockView);
                captureScreen.setImageBitmap(patternImage);
                mPatternLockView.setEnabled(true);
                savePatternSetting.setEnabled(true);
                break;
            case R.id.save_pattern_setting:
                saveSettingToDb();
                break;
        }
    }

    /** Method for saving the created configuration setting into the app database
     */
    private void saveSettingToDb(){
        numCols = patternSharedPreferences.getInt("numCols",3);
        numRows = patternSharedPreferences.getInt("numRows", 3);
        int maxTrials = patternSharedPreferences.getInt("maxTrials", 3);
        dotSize = patternSharedPreferences.getInt("dotSize", 30);
        patternLength = patternSharedPreferences.getInt("patternLength", numRows * numCols);
        vibration = patternSharedPreferences.getBoolean("vibration", true);
        boolean stealth = patternSharedPreferences.getBoolean("stealth", false);

        PatternConfigParams params = new PatternConfigParams(numRows, numCols,dotSize,
                maxTrials,patternLength,vibration,stealth,patternImage, patternString);
        appViewModel.insertPatternLockConfigParams(params);

        //display save success notification to user
        Activity[] activities = new Activity[]{new SchemeAdminActivity(), new CreateSettings()};
        Alert.configSavedMsg(CreatePattern.this, activities);

    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(CreatePattern.this, SchemeAdminActivity.class);
        finish();
        startActivity(intent);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home_button) {

            Intent i = new Intent(CreatePattern.this, HomeUI.class);
            finish();
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
