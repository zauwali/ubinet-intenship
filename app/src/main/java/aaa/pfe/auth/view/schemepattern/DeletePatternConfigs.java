package aaa.pfe.auth.view.schemepattern;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import aaa.pfe.auth.DeleteSetups;
import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PatternConfigParams;

/** The activity for the deletion of pattern configuration setups
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class DeletePatternConfigs extends AppCompatActivity {

    private List<PatternConfigParams> patternSetups;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_pattern_config_params_list);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final DeletePatternConfigsAdapter adapter = new DeletePatternConfigsAdapter(this);
        final RecyclerView recyclerView = findViewById(R.id.delete_pattern_recyler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        final TextView titleTextView = findViewById(R.id.title_textview);
        final TextView empty_view = findViewById(R.id.empty_view);

        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);
        appViewModel.getAllPatternLockConfigParams().observe(this, new Observer<List<PatternConfigParams>>() {
            @Override
            public void onChanged(@Nullable List<PatternConfigParams> patternConfigParams) {
                adapter.setConfigurations(patternConfigParams);

                patternSetups = patternConfigParams;
                if (patternSetups.size()  == 0){
                    titleTextView.setVisibility(View.GONE);
                    empty_view.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else {
                    titleTextView.setVisibility(View.VISIBLE);
                    empty_view.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(DeletePatternConfigs.this, DeleteSetups.class);
        finish();
        startActivity(intent);
        return true;
    }
}
