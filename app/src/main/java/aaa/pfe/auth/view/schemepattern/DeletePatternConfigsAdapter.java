package aaa.pfe.auth.view.schemepattern;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PatternConfigParams;

/** This class manages the creation of configuration setups
 * for the 3 authentication schemes
 * Author: Zauwali S. Paki
 * April 2018
 */

public class DeletePatternConfigsAdapter extends RecyclerView.Adapter<DeletePatternConfigsAdapter.ViewHolder>{

    private List<PatternConfigParams> mPatternConfigParams;
    private Context context;
    private AppViewModel appViewModel;

    DeletePatternConfigsAdapter(Context context) {

        this.context = context;
        Application application = new Application();
        appViewModel = new AppViewModel(application);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_pattern_configs_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final PatternConfigParams configParams = mPatternConfigParams.get(position);
        final StringBuilder builder = new StringBuilder();

        final int configId = configParams.getConfigId();
        final int numRows = configParams.getNumRows();
        final int numCols = configParams.getNumCols();
        final int patternLength = configParams.getPatternLength();
        final int maxTrials = configParams.getMaxTrials();
        final int dotsSize = configParams.getDotsSize();
        final String vibration = configParams.isVibration()? "ON" : "OFF";
        final String stealth = configParams.isStealth()? "ON" : "OFF";

        builder.append("Configuration Id: " + configId + "\n");
        builder.append("Number of Rows: " + numRows + "\n");
        builder.append("Number of Columns: " + numCols + "\n");
        builder.append("Pattern Length: " + patternLength + "\n");
        builder.append("Number of Tries: " + maxTrials + "\n");
        builder.append("Size of Dots: " + dotsSize + "\n");
        builder.append("Vibration: " + vibration + "\n");
        builder.append("Stealth: " + stealth);


        // Display the setting in a textview
        holder.patternParamsTextview.setText(builder);
        holder.patternImage.setImageBitmap(configParams.getPatternImage());

        holder.RelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Deleting a Pattern Config");
                alert.setMessage("Do you want to delete the selected config?");
                alert.setIcon(R.drawable.ic_warning_teal_dark);
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appViewModel.deletePatternConfig(configParams.getConfigId());
                        Toast.makeText(context,"Config Setup deleted", Toast.LENGTH_LONG).show();
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = alert.create();
                dialog.show();

            }
        });

    }

    void setConfigurations(List<PatternConfigParams> configurations){
        mPatternConfigParams = configurations;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mPatternConfigParams == null){
            return 0;
        }else {
            return mPatternConfigParams.size();
        }
    }

     class ViewHolder extends RecyclerView.ViewHolder{
         private TextView patternParamsTextview;
         private ImageView patternImage;
         private RelativeLayout RelativeLayout;
         public ViewHolder(View itemView) {
             super(itemView);
             patternParamsTextview = itemView.findViewById(R.id.pattern_params_textview);
             patternImage = itemView.findViewById(R.id.pattern_imageview);
             RelativeLayout = itemView.findViewById(R.id.layout_pattern_configs_content);
         }
    }
}
