package aaa.pfe.auth.view.schemepattern;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.RunExperiment;
import database.AppViewModel;
import database.PatternConfigParams;

/** The activity for the display of pattern setups
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class PatternConfigs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_lock_config_params_list);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.pattern_configs_list_activity_title);

        final PatternConfigsAdapter adapter = new PatternConfigsAdapter(this);

        RecyclerView recyclerView = findViewById(R.id.pattern_lock_configs_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //observer for the list of pattern lock settings
        AppViewModel appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);
        appViewModel.getAllPatternLockConfigParams().observe(this, new Observer<List<PatternConfigParams>>() {
            @Override
            public void onChanged(@Nullable List<PatternConfigParams> patternConfigParams) {
                adapter.setConfigurations(patternConfigParams);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home_button) {
            Intent i = new Intent(PatternConfigs.this, HomeUI.class);
            finish();
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(PatternConfigs.this, RunExperiment.class);
        startActivity(intent);
        finish();
        return true;
    }
}
