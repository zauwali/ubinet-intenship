package aaa.pfe.auth.view.schemepattern;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import aaa.pfe.auth.R;
import database.AppViewModel;
import database.PatternConfigParams;

/** Adapter class for PatternCofigs
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class PatternConfigsAdapter extends RecyclerView.Adapter<PatternConfigsAdapter.ViewHolder>{

    private static final String TAG = "PatternLockConfigParams";
    private List<PatternConfigParams> mPatternConfigParams;
    private Context context;
    private Application application;
    private AppViewModel appViewModel;

    //for keeping the default/current pin code configuration
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public PatternConfigsAdapter(Context context) {

        this.context = context;
        application = new Application();
        appViewModel = new AppViewModel(application);
        preferences = context.getApplicationContext().getSharedPreferences(
                context.getString(R.string.pattern_lock_shared_preference), context.MODE_PRIVATE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_pattern_configs_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final PatternConfigParams configParams = mPatternConfigParams.get(position);
        final StringBuilder builder = new StringBuilder();

        final int configId = configParams.getConfigId();
        final int numRows = configParams.getNumRows();
        final int numCols = configParams.getNumCols();
        final int patternLength = configParams.getPatternLength();
        final int maxTrials = configParams.getMaxTrials();
        final int dotsSize = configParams.getDotsSize();
        final String vibration = configParams.isVibration()? "ON" : "OFF";
        final String stealth = configParams.isStealth()? "ON" : "OFF";

        builder.append("Configuration Id: " + configId + "\n");
        builder.append("Number of Rows: " + numRows + "\n");
        builder.append("Number of Columns: " + numCols + "\n");
        builder.append("Pattern Length: " + patternLength + "\n");
        builder.append("Number of Tries: " + maxTrials + "\n");
        builder.append("Size of Dots: " + dotsSize + "\n");
        builder.append("Vibration: " + vibration + "\n");
        builder.append("Stealth: " + stealth);


        // Display the setting in a textview
        holder.patternParamsTextview.setText(builder);

        holder.patternImage.setImageBitmap(configParams.getPatternImage());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor = preferences.edit();

                editor.putInt("numRows", numRows);
                editor.putInt("numCols", numCols);
                editor.putInt("patternLength", patternLength);
                editor.putInt("maxTrials", maxTrials);
                editor.putBoolean("vibration", configParams.isVibration());
                editor.putBoolean("stealth", configParams.isStealth());
                editor.putInt("dotSize", dotsSize);
                editor.putString("patternString", configParams.getPatternString());
                editor.putInt("configId",configParams.getConfigId());

                //remove the previously set configuration before setting the current one
                editor.remove(context.getString(R.string.pattern_lock_shared_preference));
                editor.apply();

                Intent intent = new Intent(context, RunPatternExperiment.class);
                ((Activity)context).finish();
                context.startActivity(intent);
            }
        });
    }

    void setConfigurations(List<PatternConfigParams> configurations){
        mPatternConfigParams = configurations;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mPatternConfigParams == null){
            return 0;
        }else {
            return mPatternConfigParams.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView patternParamsTextview;
        private ImageView patternImage;
        private RelativeLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            patternParamsTextview = itemView.findViewById(R.id.pattern_params_textview);
            patternImage = itemView.findViewById(R.id.pattern_imageview);
            linearLayout = itemView.findViewById(R.id.layout_pattern_configs_content);

        }
    }
}
