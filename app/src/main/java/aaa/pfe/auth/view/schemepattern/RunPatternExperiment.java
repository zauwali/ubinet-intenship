package aaa.pfe.auth.view.schemepattern;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import aaa.pfe.auth.utils.Alert;
import aaa.pfe.auth.HomeUI;
import aaa.pfe.auth.R;
import aaa.pfe.auth.RunExperiment;
import aaa.pfe.auth.utils.LogData;
import aaa.pfe.auth.utils.TimeAndDate;
import aaa.pfe.auth.view.patternlockview.PatternLockView;
import aaa.pfe.auth.view.patternlockview.listener.PatternLockViewListener;
import aaa.pfe.auth.view.patternlockview.utils.PatternLockUtils;
import database.AppViewModel;
import database.PatternUserId;
import de.hdodenhof.circleimageview.CircleImageView;

/** The activity for performing experiment with pattern lock
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class RunPatternExperiment extends AppCompatActivity {

    private PatternLockView mPatternLockView;

    private TextView configIdTextView, patternHeader, setupIdLabelTextview;

    private CircleImageView startExperiment;

    SharedPreferences patternSharedPreferences;

    private String patternString;
    private ArrayList<LogData> logs = new ArrayList<>();

    private int numTrial = 1;
    private int maxTrials = 3;

    private int userId, configId;

    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {

        //private long begin;
        @Override
        public void onStarted() {
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {

            LogData logData = new LogData(configIdTextView.getText().toString(),
                    configIdTextView.getText().toString() + "." + userId,
                    getString(R.string.pattern_size_drawn_sofar) + progressPattern.size(),String.valueOf(numTrial),
                    TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
            logs.add(logData);
        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern, boolean sizeReached) {

            LogData logData = new LogData(configIdTextView.getText().toString(),
                    configIdTextView.getText().toString() + "." + userId,
                    getString(R.string.pattern_drawing_completed), String.valueOf(numTrial),
                    TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

            logs.add(logData);

            String result = PatternLockUtils.patternToSha1(mPatternLockView, pattern);

            if(sizeReached){

                mPatternLockView.setEnabled(false);
            }

            if (patternSharedPreferences.contains("patternString")){
                patternString = patternSharedPreferences.getString("patternString","");

                if (result.equals(patternString)){
                    logData = new LogData(configIdTextView.getText().toString(),
                            configIdTextView.getText().toString() + "." + userId,
                            getString(R.string.correct_pattern), String.valueOf(numTrial),
                            TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                    logs.add(logData);
                    userFinishExperiment();
                    insertLastUserId(configId, userId);
                    //hideViews();
                    setViewsAfterExperiment();
                    try {

                        LogData.writeLog(logs,getString(R.string.pattern_log_file_name));
                        logs.clear();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Alert.showCorrectPatternMsg(RunPatternExperiment.this);
                    numTrial = 1;

                }else {
                    if(numTrial == maxTrials){
                        Alert.maxAttemptsMsg(RunPatternExperiment.this);
                        //hideViews();
                        setViewsAfterExperiment();
                        logData = new LogData(configIdTextView.getText().toString(),
                                configIdTextView.getText().toString() + "." + userId,
                                getString(R.string.wrong_pattern), String.valueOf(numTrial),
                                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());

                        logs.add(logData);
                        userFinishExperiment();
                        insertLastUserId(configId, userId);
                        numTrial = 1;

                        try {
                            LogData.writeLog(logs,getString(R.string.pattern_log_file_name));
                            logs.clear();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }else {
                        mPatternLockView.setEnabled(true);
                        logData = new LogData(configIdTextView.getText().toString(),
                                configIdTextView.getText().toString() + "." + userId,
                                getString(R.string.wrong_pattern),String.valueOf(numTrial),
                                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
                        logs.add(logData);

                        try {
                            LogData.writeLog(logs,getString(R.string.pattern_log_file_name));
                            logs.clear();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Alert.showWrongPatternMsg(RunPatternExperiment.this);
                        numTrial++;
                    }

                }
            }
        }
        @Override
        public void onCleared() {
        }

        @Override
        public void onSizeReached(List<PatternLockView.Dot> pattern) {

        }
    };

    private AppViewModel appViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheme_pattern);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.pattern_lock_auth_window_title);

        appViewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        patternSharedPreferences = getApplicationContext().getSharedPreferences(
                getString(R.string.pattern_lock_shared_preference), MODE_PRIVATE);

        mPatternLockView =  findViewById(R.id.pattern_lock_view);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);

        configIdTextView = findViewById(R.id.setup_id_textview);
        patternHeader = findViewById(R.id.verify_pattern_header);
        setupIdLabelTextview = findViewById(R.id.setup_id_label_textview);

        startExperiment = findViewById(R.id.start_experiment);
        startExperiment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUserIdDialog();
            }
        });

        /*
        userIdSpinner = findViewById(R.id.users_id_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.users_id, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userIdSpinner.setAdapter(adapter);
        */
        setAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAllPreferences();
    }

    private void setAllPreferences() {


        int numRows = patternSharedPreferences.getInt("numRows",3);

        int numCols = patternSharedPreferences.getInt("numCols",3);

        mPatternLockView.setDotCount(numCols, numRows);

        int patternLength = patternSharedPreferences.getInt("patternLength",9);
        mPatternLockView.setPatternMaxSize(patternLength + 1);

        maxTrials = patternSharedPreferences.getInt("maxTrials",3);

        boolean vibration = patternSharedPreferences.getBoolean("vibration",true);
        mPatternLockView.setTactileFeedbackEnabled(vibration);

        boolean stealth = patternSharedPreferences.getBoolean("stealth",false);
        mPatternLockView.setInStealthMode(stealth);

        int dotSize = patternSharedPreferences.getInt("dotSize",30);
        mPatternLockView.setDotNormalSize(dotSize);

        configId = patternSharedPreferences.getInt("configId", 0);
        configIdTextView.setText(String.valueOf(configId));

        mPatternLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_FREE);

        patternString = patternSharedPreferences.getString("patternString", "");

    }


    @Override
    public boolean onSupportNavigateUp(){
        Intent intent = new Intent(RunPatternExperiment.this, RunExperiment.class);
        finish();
        startActivity(intent);
        return true;
    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home_button) {

            Intent i = new Intent(RunPatternExperiment.this, HomeUI.class);
            finish();
            startActivity(i);

        }
        if (id == R.id.load_settings){
            Intent intent = new Intent(RunPatternExperiment.this, PatternConfigs.class);
            finish();
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void userStartsExperiment(){
      LogData  logData = new LogData(configIdTextView.getText().toString(),
              configIdTextView.getText().toString() + "." + userId,
                getString(R.string.user_starts_experiment),String.valueOf(numTrial),
              TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    private void userFinishExperiment(){
        LogData  logData = new LogData(configIdTextView.getText().toString(),
                configIdTextView.getText().toString() + "." + userId,
                getString(R.string.user_finish_experiment),String.valueOf(numTrial),
                TimeAndDate.getCurrentTime(), TimeAndDate.getCurrentDate());
        logs.add(logData);
    }

    private void showViews(){
        mPatternLockView.setVisibility(View.VISIBLE);
        patternHeader.setVisibility(View.VISIBLE);
        mPatternLockView.clearPattern();
    }

    private void hideViews(){
        mPatternLockView.setVisibility(View.INVISIBLE);
        patternHeader.setVisibility(View.INVISIBLE);
        startExperiment.setVisibility(View.VISIBLE);
        mPatternLockView.setEnabled(true);
    }

    private void setViews4Experiment(){
        startExperiment.setVisibility(View.INVISIBLE);
        mPatternLockView.setVisibility(View.VISIBLE);
        patternHeader.setVisibility(View.VISIBLE);
        mPatternLockView.clearPattern();
        configIdTextView.setVisibility(View.INVISIBLE);
        setupIdLabelTextview.setVisibility(View.INVISIBLE);
    }

    private void setViewsAfterExperiment(){
        mPatternLockView.setVisibility(View.INVISIBLE);
        patternHeader.setVisibility(View.INVISIBLE);
        startExperiment.setVisibility(View.VISIBLE);
        mPatternLockView.setEnabled(true);
        configIdTextView.setVisibility(View.VISIBLE);
        setupIdLabelTextview.setVisibility(View.VISIBLE);

    }

    /** This displays a dialog with the id of the user that
     * will perform the experiment for the experimenter each
     * time the experiment starts
     **/
    private void showUserIdDialog(){

        // Get the last used user Id and build up it
        try {
            userId = getLastUserId(configId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.layout_user_id_alert_dialog,null);
        alert.setView(dialogView);

        TextView userIdTextview = dialogView.findViewById(R.id.user_id_textview);
        userIdTextview.setText(configIdTextView.getText().toString() + "." + userId);
        alert.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setViews4Experiment();
                userStartsExperiment();
            }
        });
        alert.create().show();
    }

    private int getLastUserId(int configId) throws ExecutionException, InterruptedException {
        //getting the last user id from the app database
        PatternUserId patternUserId = appViewModel.getLastPatternUserId(configId);
        if (patternUserId != null){
            return patternUserId.getLastUserId() + 1;
        }
        // if this is the first user to do the experiment
        else {
            return 1;
        }
    }

    private void insertLastUserId(int configId, int userId){
        PatternUserId patternUserId = new PatternUserId(configId, userId);
        appViewModel.insertLastPatternUserId(patternUserId);
    }
}
