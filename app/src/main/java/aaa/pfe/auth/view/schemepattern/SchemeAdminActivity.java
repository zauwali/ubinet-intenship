package aaa.pfe.auth.view.schemepattern;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import aaa.pfe.auth.CreateSettings;
import aaa.pfe.auth.R;
import aaa.pfe.auth.view.mother.AdminActivity;

/**
 * Created by Anasse on 21/11/2017.
 * Updated by Zauwali S. Paki April 2018
 */

public class SchemeAdminActivity extends AdminActivity {

    private EditText nbRowsEditText;
    private EditText nbColumnsEditText;
    private SharedPreferences sharedPreferences;
    private EditText lengthEditText;
    private EditText maxTryEditText;
    private CheckBox vibrationBox;
    private CheckBox stealthBox;
    private EditText dotSizeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheme_admin);

        Toolbar myToolbar =  findViewById(R.id.common_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.pattern_lock_settings_window_title);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        sharedPreferences = getApplicationContext().getSharedPreferences(
                getString(R.string.pattern_lock_shared_preference), MODE_PRIVATE);
        nbRowsEditText =  findViewById(R.id.nbRows);
        nbColumnsEditText =  findViewById(R.id.nbColumns);
        lengthEditText = findViewById(R.id.lengthPattern);
        maxTryEditText = findViewById(R.id.maxTry);
        vibrationBox = findViewById(R.id.vibration);
        stealthBox = findViewById(R.id.stealth);
        dotSizeEditText = findViewById(R.id.dotSize);

        setDefaultsValues();

    }

    private void setDefaultsValues() {
            int numRows = sharedPreferences.getInt("numRows",3);
            nbRowsEditText.setText(String.valueOf(numRows));

            int numCols = sharedPreferences.getInt("numCols",3);
            nbColumnsEditText.setText(String.valueOf(numCols));

            int patternLength = sharedPreferences.getInt("patternLength",4);
            lengthEditText.setText(String.valueOf(patternLength));

            int maxTrials = sharedPreferences.getInt("maxTrials",3);
            maxTryEditText.setText(String.valueOf(maxTrials));

            boolean vibration = sharedPreferences.getBoolean("vibration",true);
            vibrationBox.setChecked(vibration);

            boolean stealth = sharedPreferences.getBoolean("stealth",false);
            stealthBox.setChecked(stealth);

            int dotSize = sharedPreferences.getInt("dotSize",30);
            dotSizeEditText.setText(String.valueOf(dotSize));

    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(SchemeAdminActivity.this, CreateSettings.class);
        finish();
        startActivity(intent);
        return true;
    }

    @Override
    public void retrieveChanges(View v) {
        Toast t = Toast.makeText(this, "SaveScheme", Toast.LENGTH_SHORT);
        saveChanges();
        t.show();
    }

    @Override
    public void saveChanges() {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        int numRows = Integer.valueOf(nbRowsEditText.getText().toString());
        int numCols = Integer.valueOf(nbColumnsEditText.getText().toString());
        int patternLength = Integer.valueOf(lengthEditText.getText().toString());
        if(patternLength >= (numRows * numCols)){
            patternLength = (numRows * numCols) - 1;
        }
        int maxTrials = Integer.valueOf(maxTryEditText.getText().toString());
        int dotsSize = Integer.valueOf(dotSizeEditText.getText().toString());
        boolean vibration = vibrationBox.isChecked();
        boolean stealth = stealthBox.isChecked();

        editor.putInt("numRows", numRows);
        editor.putInt("numCols", numCols);
        editor.putInt("patternLength", patternLength);
        editor.putInt("maxTrials", maxTrials);
        editor.putBoolean("vibration", vibration);
        editor.putBoolean("stealth", stealth);
        editor.putInt("dotSize", dotsSize);

        editor.remove(getString(R.string.pattern_lock_shared_preference));
        editor.apply();
        displayMessage();
    }
    private void displayMessage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(SchemeAdminActivity.this);
        builder
                .setTitle("Parameters saved")
                .setIcon(R.drawable.ic_info_teal)
                .setMessage("You are now going create pattern for this configuration")
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(SchemeAdminActivity.this,CreatePattern.class);
                        finish();
                        startActivity(i);
                    }
                })
                .create()
                .show();
    }
}
