package aaa.pfe.auth.view.schemepattern;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;

/** The class for taking the screen capture of a drawn pattern
 * Author: Zauwali S. Paki
 * April 2018
 * */

public class TakePatternScreenShot {
    public static Bitmap takeScreenShot(View view){
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return b;
    }

    public static Bitmap takeScreenShotOfView(View view){

        Bitmap oldBitmap = takeScreenShot(view);
        // Create new bitmap based on the size and config of the old
        Bitmap newBitmap = Bitmap.createBitmap(oldBitmap.getWidth(),
                oldBitmap.getHeight(), oldBitmap.getConfig());
        // Instantiate a canvas and prepare it to paint to the new bitmap
        Canvas canvas = new Canvas(newBitmap);

        // Paint it white (or whatever color you want)
        canvas.drawColor(Color.WHITE);

        // Draw the old bitmap ontop of the new white one
        canvas.drawBitmap(oldBitmap, 0, 0, null);
        return newBitmap;
    }
}
