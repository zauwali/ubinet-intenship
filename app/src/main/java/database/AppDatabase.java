package database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

/** The app database
 * Author: Zauwali S. Paki
 * April 2018
 */
@Database(entities = {PassFaceConfigParams.class,PatternConfigParams.class,
        PincodeConfigParams.class, PincodUserId.class,
        PassfacesUserId.class, PatternUserId.class}, version = 1)
@TypeConverters({BitmapTypeConverter.class, ArrayListConverter.class})

public abstract class AppDatabase extends RoomDatabase {

    public abstract PassFaceConfigParamsDao passFaceConfigParamsDao();
    public abstract PatternConfigParamsDao patternLockConfigParamsDao();
    public abstract PincodeConfigParamsDao pincodeConfigParamsDao();
    public abstract PincodUserIdDao pincondUserIdDao();
    public abstract PassfacesUserIdDao passfacesUserIdDao();
    public abstract PatternUserIdDao patternUserIdDao();

    private static AppDatabase INSTANCE;

    static AppDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class,"app_database")
                            //.addCallback(sPinCodeCallback)
                            .fallbackToDestructiveMigration()
                            //.allowMainThreadQueries()
                            .build();
                }

            }
        }
        return INSTANCE;
    }
}
