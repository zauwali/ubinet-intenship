package database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.ExecutionException;

/** This class contains the app data: authentication passwords
 * and configuration parameters for all the authentication mechanisms
 * ( pincode, pattern lock, and face pass)
 * It caches the latest data from the app database for the
 * UIs to access via the AppVieModel class
 * that serves as the data communication center between
 * the UIs and the data source
 *
 * Author: Zauwali S. Paki
 * April 2018
 */

public class AppRepository {
    //Face Pass
    private PassFaceConfigParamsDao mPassFaceConfigParamsDao;
    private LiveData<List<PassFaceConfigParams>> mAllFacePassConfigParams;

    // Pattern Lock
    private PatternConfigParamsDao mPatternConfigParamsDao;
    private LiveData<List<PatternConfigParams>> mAllPatternLockConfigParams;


    private PincodeConfigParamsDao mPincodeConfigParamsDao;
    private PincodUserIdDao mPincodeUserIdDao;
    private PassfacesUserIdDao mPassfacesUserIdDao;
    private PatternUserIdDao mPatternUserIdDao;



    AppRepository(Application application){
        AppDatabase db = AppDatabase.getDatabase(application);
        mPassFaceConfigParamsDao = db.passFaceConfigParamsDao();
        mAllFacePassConfigParams = mPassFaceConfigParamsDao.getAllConfigParams();

        mPatternConfigParamsDao = db.patternLockConfigParamsDao();
        mAllPatternLockConfigParams = mPatternConfigParamsDao.getAllConfigParams();

        mPincodeConfigParamsDao = db.pincodeConfigParamsDao();

        mPincodeUserIdDao = db.pincondUserIdDao();
        mPassfacesUserIdDao = db.passfacesUserIdDao();
        mPatternUserIdDao = db.patternUserIdDao();

    }

    // Face Pass
    LiveData<List<PassFaceConfigParams>> getAllFacePassConfigParams(){return mAllFacePassConfigParams;}
    public void deleteAllFacePassConfigParams(){
        mPassFaceConfigParamsDao.deleteAllConfigParams();}

    public List<PassFaceConfigParams> getAllPassfaceSetupsList() throws ExecutionException, InterruptedException {
        GetAllPassfaceSetupsList setupsList = new GetAllPassfaceSetupsList(mPassFaceConfigParamsDao);
        setupsList.execute();
        return setupsList.get();
    }

    private static class GetAllPassfaceSetupsList extends AsyncTask<Void, Void, List<PassFaceConfigParams>>{

        private PassFaceConfigParamsDao dao;
        GetAllPassfaceSetupsList(PassFaceConfigParamsDao dao){this.dao = dao;}

        @Override
        protected List<PassFaceConfigParams> doInBackground(Void... voids) {
            return dao.getAllSetupList();
        }

        @Override
        protected void onPostExecute(List<PassFaceConfigParams> passFaceConfigParams) {
            super.onPostExecute(passFaceConfigParams);
        }
    }

    public List<PincodeConfigParams> getAllPincodeSetupsList() throws ExecutionException, InterruptedException {
        GetAllPincodeSetupsList setupsList = new GetAllPincodeSetupsList(mPincodeConfigParamsDao);
        setupsList.execute();
        return setupsList.get();
    }

    private static class GetAllPincodeSetupsList extends AsyncTask<Void, Void, List<PincodeConfigParams>>{

        private PincodeConfigParamsDao dao;
        GetAllPincodeSetupsList(PincodeConfigParamsDao dao){this.dao = dao;}
        @Override
        protected List<PincodeConfigParams> doInBackground(Void... voids) {
            return dao.getAllSetupsList();
        }

        @Override
        protected void onPostExecute(List<PincodeConfigParams> pincodeConfigParams) {
            super.onPostExecute(pincodeConfigParams);
        }
    }

    public void deletePincodeConfig(int configId){
        new DeletePincodeConfig(mPincodeConfigParamsDao).execute(configId);
    }

    private static class DeletePincodeConfig extends AsyncTask<Integer, Void, Void>{

        PincodeConfigParamsDao dao;
        DeletePincodeConfig(PincodeConfigParamsDao dao){ this.dao = dao; }
        @Override
        protected Void doInBackground(Integer... integers) {
            dao.deletePincodeConfig(integers[0]);
            return null;
        }
    }

    public List<PatternConfigParams> getAllPatternSetupsList() throws ExecutionException, InterruptedException {
        GetAllPatternSetupsList setupsList = new GetAllPatternSetupsList(mPatternConfigParamsDao);
        setupsList.execute();
        return setupsList.get();
    }

    private static class GetAllPatternSetupsList extends AsyncTask<Void, Void, List<PatternConfigParams>>{

        private PatternConfigParamsDao dao;
        GetAllPatternSetupsList(PatternConfigParamsDao dao){this.dao = dao;}
        @Override
        protected List<PatternConfigParams> doInBackground(Void... voids) {
            return dao.getAllSetupList();
        }

        @Override
        protected void onPostExecute(List<PatternConfigParams> patternConfigParams) {
            super.onPostExecute(patternConfigParams);
        }
    }

    public void deletePatternConfig(int configId){
        new DeletePatternConfig(mPatternConfigParamsDao).execute(configId);
    }
    private static class DeletePatternConfig extends AsyncTask<Integer, Void, Void>{

        private PatternConfigParamsDao dao;
        DeletePatternConfig(PatternConfigParamsDao dao){ this.dao = dao; }
        @Override
        protected Void doInBackground(Integer... integers) {
            dao.deletePatternConfig(integers[0]);
            return null;
        }
    }

    // Adding a new Face Pass Configuration parameters
    public void insertFacePassConfigParams(PassFaceConfigParams configParams){
        new InsertFacePassConfigParams(mPassFaceConfigParamsDao).execute(configParams);
    }
    // class for adding the configuration params in a background process
    private static class InsertFacePassConfigParams extends AsyncTask<PassFaceConfigParams, Void, Void>{
        private PassFaceConfigParamsDao dao;
        InsertFacePassConfigParams(PassFaceConfigParamsDao dao){this.dao = dao;}
        @Override
        protected Void doInBackground(PassFaceConfigParams... passFaceConfigParams) {
            dao.insertFacePassConfigParams(passFaceConfigParams[0]);
            return null;
        }
    }

    public void deletePassfaceConfig(int configId){
        new DeletePassfaceConfig(mPassFaceConfigParamsDao).execute(configId);
    }

    private static class DeletePassfaceConfig extends AsyncTask<Integer, Void, Void>{

        PassFaceConfigParamsDao dao;
        DeletePassfaceConfig(PassFaceConfigParamsDao dao){ this.dao = dao; }
        @Override
        protected Void doInBackground(Integer... integers) {
            dao.deletePassfaceConfig(integers[0]);
            return null;
        }
    }

    LiveData<List<PatternConfigParams>> getAllPatternLockConfigParams(){return mAllPatternLockConfigParams;}
    public void deleteAllPatternLockConfigParams(){
        mPatternConfigParamsDao.deleteAllConfigParams();}

    public void insertPatternLockConfigParams(PatternConfigParams configParams){
        new InsertPatternLockConfigParams(mPatternConfigParamsDao).execute(configParams);
    }
    private static class InsertPatternLockConfigParams extends AsyncTask<PatternConfigParams, Void, Void>{

        private PatternConfigParamsDao dao;
        InsertPatternLockConfigParams(PatternConfigParamsDao dao){this.dao = dao;}
        @Override
        protected Void doInBackground(PatternConfigParams... patternConfigParams) {
            dao.insertConfigParams(patternConfigParams[0]);
            return null;
        }
    }

    public void insertPincodeSetting(PincodeConfigParams params){
        new InsertPincodeSetting(mPincodeConfigParamsDao).execute(params);
    }
    private static class InsertPincodeSetting extends AsyncTask<PincodeConfigParams, Void, Void>{

        private PincodeConfigParamsDao dao;
        InsertPincodeSetting(PincodeConfigParamsDao dao){this.dao = dao;}
        @Override
        protected Void doInBackground(PincodeConfigParams... pincodeConfigParams) {
            dao.insertConfigParams(pincodeConfigParams[0]);
            return null;
        }
    }

    public LiveData<List<PincodeConfigParams>> getAllPincodeSettings() throws ExecutionException, InterruptedException {
        GetAllPincodeSettings params = new GetAllPincodeSettings(mPincodeConfigParamsDao);
        params.execute();
        return params.get();
    }
    private static class GetAllPincodeSettings extends AsyncTask<Void, Void, LiveData<List<PincodeConfigParams>>>{

        private PincodeConfigParamsDao dao;
        GetAllPincodeSettings(PincodeConfigParamsDao dao){this.dao = dao;}
        @Override
        protected LiveData<List<PincodeConfigParams>> doInBackground(Void... voids) {
            return dao.getAllConfigParams();
        }

        @Override
        protected void onPostExecute(LiveData<List<PincodeConfigParams>> listLiveData) {
            super.onPostExecute(listLiveData);
        }
    }

    public void insertLastPincodeUserId(PincodUserId data){
        new InsertLastPincodeUserId(mPincodeUserIdDao).execute(data);
    }
    private static class InsertLastPincodeUserId extends AsyncTask<PincodUserId, Void, Void>{
        private PincodUserIdDao dao;
        public InsertLastPincodeUserId(PincodUserIdDao dao) { this.dao = dao;}

        @Override
        protected Void doInBackground(PincodUserId... pincodUserIds) {
            dao.insertLastUserId(pincodUserIds[0]);
            return null;
        }
    }

    public PincodUserId getLastPincodeUserId(int configId) throws ExecutionException, InterruptedException {
        GetLastPincodeUserId getLastPincodeUserId = new GetLastPincodeUserId(mPincodeUserIdDao);
        getLastPincodeUserId.execute(configId);
        return getLastPincodeUserId.get();
    }

    private static class GetLastPincodeUserId extends AsyncTask<Integer, Void, PincodUserId>{

        private PincodUserIdDao dao;

        public GetLastPincodeUserId(PincodUserIdDao dao) {
            this.dao = dao;
        }

        @Override
        protected PincodUserId doInBackground(Integer... integers) {
            return dao.getLastUserId(integers[0]);
        }

        @Override
        protected void onPostExecute(PincodUserId pincodUserId) {
            super.onPostExecute(pincodUserId);
        }
    }

    public void insertLastPassfacesUserId(PassfacesUserId data){
        new InsertLastPassfacesUserId(mPassfacesUserIdDao).execute(data);
    }
    private static class InsertLastPassfacesUserId extends AsyncTask<PassfacesUserId, Void, Void>{
        private PassfacesUserIdDao dao;
        public InsertLastPassfacesUserId(PassfacesUserIdDao dao) { this.dao = dao;}

        @Override
        protected Void doInBackground(PassfacesUserId... passfacesUserIds) {
            dao.insertLastUserId(passfacesUserIds[0]);
            return null;
        }
    }

    public PassfacesUserId getLastPassfacesUserId(int configId) throws ExecutionException, InterruptedException {
        GetLastPassfacesUserId getLastPassfacesUserId = new GetLastPassfacesUserId(mPassfacesUserIdDao);
        getLastPassfacesUserId.execute(configId);
        return getLastPassfacesUserId.get();
    }

    private static class GetLastPassfacesUserId extends AsyncTask<Integer, Void, PassfacesUserId>{

        private PassfacesUserIdDao dao;

        public GetLastPassfacesUserId(PassfacesUserIdDao dao) {
            this.dao = dao;
        }

        @Override
        protected PassfacesUserId doInBackground(Integer... integers) {
            return dao.getLastUserId(integers[0]);
        }

        @Override
        protected void onPostExecute(PassfacesUserId passfacesUserId) {
            super.onPostExecute(passfacesUserId);
        }
    }

    public void insertLastPatternUserId(PatternUserId data){
        new InsertLastPatternUserId(mPatternUserIdDao).execute(data);
    }
    private static class InsertLastPatternUserId extends AsyncTask<PatternUserId, Void, Void>{
        private PatternUserIdDao dao;
        public InsertLastPatternUserId(PatternUserIdDao dao) { this.dao = dao;}

        @Override
        protected Void doInBackground(PatternUserId... passfacesUserIds) {
            dao.insertLastUserId(passfacesUserIds[0]);
            return null;
        }
    }

    public PatternUserId getLastPatternUserId(int configId) throws ExecutionException, InterruptedException {
        GetLastPatternUserId getLastPatternUserId = new GetLastPatternUserId(mPatternUserIdDao);
        getLastPatternUserId.execute(configId);
        return getLastPatternUserId.get();
    }

    private static class GetLastPatternUserId extends AsyncTask<Integer, Void, PatternUserId>{

        private PatternUserIdDao dao;

        public GetLastPatternUserId(PatternUserIdDao dao) {
            this.dao = dao;
        }

        @Override
        protected PatternUserId doInBackground(Integer... integers) {
            return dao.getLastUserId(integers[0]);
        }

        @Override
        protected void onPostExecute(PatternUserId patternUserId) {
            super.onPostExecute(patternUserId);
        }
    }

}
