package database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;

/** This class serves as a data communication center
 * between the UIs and app data source.
 * It caches data from the AppRepository class
 * which has access to the app database
 *
 * Author: Zauwali S. Paki
 * April 2018
 */

public class AppViewModel extends AndroidViewModel {

    private AppRepository mAppRepository;

    public AppViewModel(Application application){
        super(application);
        mAppRepository = new AppRepository(application);
    }

    // Pass Faces
    public LiveData<List<PassFaceConfigParams>> getAllFacePassConfigParams() {
        return mAppRepository.getAllFacePassConfigParams();
    }
    public void deleteAllFacePassConfigParams(){mAppRepository.deleteAllFacePassConfigParams();}

    public void deletePincodeConfig(int configId){
        mAppRepository.deletePincodeConfig(configId);
    }
    public void deletePassfaceConfig(int configId){
        mAppRepository.deletePassfaceConfig(configId);
    }
    public void deletePatternConfig(int configId){
        mAppRepository.deletePatternConfig(configId);
    }

    public void insertFacePassConfigParams(PassFaceConfigParams configParams){
        mAppRepository.insertFacePassConfigParams(configParams);
    }

    //Pattern Lock
    public LiveData<List<PatternConfigParams>> getAllPatternLockConfigParams() {
        return mAppRepository.getAllPatternLockConfigParams();
    }
    public void deleteAllPatternLockConfigParams(){mAppRepository.deleteAllPatternLockConfigParams();}
    public void insertPatternLockConfigParams(PatternConfigParams configParams){
        mAppRepository.insertPatternLockConfigParams(configParams);
    }

    public void insertPincodeSetting(PincodeConfigParams params){
        mAppRepository.insertPincodeSetting(params);
    }
    public LiveData<List<PincodeConfigParams>> getAllPincodeSettings() throws ExecutionException, InterruptedException {
        return mAppRepository.getAllPincodeSettings();
    }
    public List<PincodeConfigParams> getAllPincodeSetupsList() throws ExecutionException, InterruptedException {
        return mAppRepository.getAllPincodeSetupsList();
    }

    public List<PassFaceConfigParams> getAllPassfaceSetupsList() throws ExecutionException, InterruptedException {
        return mAppRepository.getAllPassfaceSetupsList();
    }

    public List<PatternConfigParams> getAllPatternSetupsList() throws ExecutionException, InterruptedException {
        return mAppRepository.getAllPatternSetupsList();
    }

    public void insertLastUserId(PincodUserId data){
        mAppRepository.insertLastPincodeUserId(data);
    }

    public PincodUserId getLastUserId(int configId) throws ExecutionException, InterruptedException {
        return mAppRepository.getLastPincodeUserId(configId);
    }

    public void insertLastPassfacesUserId(PassfacesUserId data){
        mAppRepository.insertLastPassfacesUserId(data);
    }

    public PassfacesUserId getLastPassfacesUserId(int configId) throws ExecutionException, InterruptedException {
        return mAppRepository.getLastPassfacesUserId(configId);
    }

    public void insertLastPatternUserId(PatternUserId data){
        mAppRepository.insertLastPatternUserId(data);
    }

    public PatternUserId getLastPatternUserId(int configId) throws ExecutionException, InterruptedException {
        return mAppRepository.getLastPatternUserId(configId);
    }
}
