package database;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/** This class converts ArrayList to the primitive data type
 * in order to be saved in the app database.
 * Author: Zauwali S. Paki
 * May 2018
 */

public  class ArrayListConverter {

//    @TypeConverter
//    public static String[] arrayListToArray(ArrayList<String> arrayList){
//        return (String[]) arrayList.toArray();
//    }
//
//    @TypeConverter
//    public static ArrayList<String> arrayToArrayList(String[] array){
//        ArrayList<String> arrayList = new ArrayList<>();
//        for (String s: array)
//            arrayList.add(s);
//        return arrayList;
//    }

    @TypeConverter
    public static ArrayList<String> fromString(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }
    @TypeConverter
    public static String fromArrayList(ArrayList<String> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

}
