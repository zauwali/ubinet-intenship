package database;

import android.arch.persistence.room.TypeConverter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/** This class converts Bitmap to array data type
 * in order to be saved in the app database. Bitmap is used to
 * store image types
 * Author: Zauwali S. Paki
 * May 2018
 */

public class BitmapTypeConverter {
    @TypeConverter
    public byte[] bitmapToByteArray(Bitmap bitmap){

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        return stream.toByteArray();
    }

    @TypeConverter
    public Bitmap byteArrayToBitmap(byte[] bytes){

        InputStream inputStream = new ByteArrayInputStream(bytes);
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeStream(inputStream, null, options);

    }
}
