package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/** This class is used for keeping the configuration setups for
 * Passfaces authentication mechanisms.
 *
 * Author: Zauwali Sabitu Paki
 * April 2018
 */

@Entity(indices = {@Index(value = {"numPhotosShown", "numPhotos","typePhoto", "password",
        "shufflePhotos", "usePhotoMultipleTimes", "maxTrials"}, unique = true)})
public class PassFaceConfigParams {

    @PrimaryKey(autoGenerate = true)
    int configId;
    private int numPhotosShown;
    private int numPhotos;
    private int maxTrials;
    @NonNull
    private String typePhoto;
    @NonNull
    private ArrayList<String> password = new ArrayList<>();
    private boolean shufflePhotos;
    private boolean usePhotoMultipleTimes;

    public PassFaceConfigParams(int numPhotosShown, int numPhotos, @NonNull String typePhoto,
                                boolean shufflePhotos, boolean usePhotoMultipleTimes, @NonNull ArrayList<String> password, int maxTrials) {
        this.numPhotosShown = numPhotosShown;
        this.numPhotos = numPhotos;
        this.typePhoto = typePhoto;
        this.shufflePhotos = shufflePhotos;
        this.usePhotoMultipleTimes = usePhotoMultipleTimes;
        this.password.addAll(password);
        this.maxTrials = maxTrials;
    }

    public int getNumPhotosShown() {
        return numPhotosShown;
    }

    public void setNumPhotosShown(int numPhotosShown) {
        this.numPhotosShown = numPhotosShown;
    }

    public int getNumPhotos() {
        return numPhotos;
    }

    public void setNumPhotos(int numPhotos) {
        this.numPhotos = numPhotos;
    }

    @NonNull
    public String getTypePhoto() {
        return typePhoto;
    }

    @NonNull
    public ArrayList<String> getPassword() {
        return password;
    }

    public void setPassword(@NonNull ArrayList<String> password) {
        this.password = password;
    }

    public void setTypePhoto(@NonNull String typePhoto) {
        this.typePhoto = typePhoto;
    }

    public boolean isShufflePhotos() {
        return shufflePhotos;
    }

    public void setShufflePhotos(boolean shufflePhotos) {
        this.shufflePhotos = shufflePhotos;
    }

    public boolean isUsePhotoMultipleTimes() {
        return usePhotoMultipleTimes;
    }

    public void setUsePhotoMultipleTimes(boolean usePhotoMultipleTimes) {
        this.usePhotoMultipleTimes = usePhotoMultipleTimes;
    }

    public int getMaxTrials() {
        return maxTrials;
    }

    public void setMaxTrials(int maxTrials) {
        this.maxTrials = maxTrials;
    }

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }
}
