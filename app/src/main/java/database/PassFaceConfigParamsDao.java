package database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/** This class is used by the Room database engine to create the
 * necessary SQL statement for storing and retrieving the
 * Passfaces authentication mechanism's setups data from the app database.
 *
 * Author: Zauwali Sabitu Paki
 * April 2018
 */
@Dao
public interface PassFaceConfigParamsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFacePassConfigParams(PassFaceConfigParams configParams);

    @Query("SELECT * FROM PassFaceConfigParams ORDER BY configId ASC")
    LiveData<List<PassFaceConfigParams>> getAllConfigParams();

    @Query("SELECT * FROM PassFaceConfigParams ORDER BY configId ASC")
    List<PassFaceConfigParams> getAllSetupList();

    @Query("DELETE FROM PassFaceConfigParams")
    void deleteAllConfigParams();

    @Query("Delete from PassFaceConfigParams WHERE configId = :configId")
    void deletePassfaceConfig(int configId);
}
