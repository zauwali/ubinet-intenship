package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/** Data Acces Objet class used by the Room engine
 * to create sql statements for various queries
 * Author: Zauwali S. Paki
 * April 2018
 */
@Dao
public interface PassfacesUserIdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLastUserId(PassfacesUserId data);

    @Query("SELECT * FROM PassfacesUserId Where configId = :configId")
    PassfacesUserId getLastUserId(int configId);

    @Query("DELETE FROM PassfacesUserId Where configId = :configId")
    void deleteLastUserId(int configId);
}
