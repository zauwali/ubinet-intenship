package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

/** This class is used for keeping the configuration setups for
 * Pattern authentication mechanism.
 *
 * Author: Zauwali Sabitu Paki
 * April 2018
 */

@Entity(indices = {@Index(value = {"numRows", "numCols", "dotsSize","maxTrials", "patternLength",
        "vibration", "stealth"}, unique = true)})
public class PatternConfigParams {

    @PrimaryKey(autoGenerate = true)
    int configId;
    private int numRows;
    private int numCols;
    private int dotsSize;
    private int maxTrials;
    private int patternLength;
    private boolean vibration;
    private boolean stealth;
    private Bitmap patternImage;
    @NonNull
    private String patternString;

    public PatternConfigParams(int numRows, int numCols, int dotsSize, int maxTrials, int patternLength,
                               boolean vibration, boolean stealth, Bitmap patternImage, @NonNull String patternString) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.dotsSize = dotsSize;
        this.maxTrials = maxTrials;
        this.patternLength = patternLength;
        this.vibration = vibration;
        this.stealth = stealth;
        this.patternImage = patternImage;
        this.patternString = patternString;
    }

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public int getNumRows() {
        return numRows;
    }

    public void setNumRows(int numRows) {
        this.numRows = numRows;
    }

    public int getNumCols() {
        return numCols;
    }

    public void setNumCols(int numCols) {
        this.numCols = numCols;
    }

    public int getDotsSize() {
        return dotsSize;
    }

    public void setDotsSize(int dotsSize) {
        this.dotsSize = dotsSize;
    }

    public int getMaxTrials() {
        return maxTrials;
    }

    public void setMaxTrials(int maxTrials) {
        this.maxTrials = maxTrials;
    }

    public int getPatternLength() {
        return patternLength;
    }

    public void setPatternLength(int patternLength) {
        this.patternLength = patternLength;
    }

    public boolean isVibration() {
        return vibration;
    }

    public void setVibration(boolean vibration) {
        this.vibration = vibration;
    }

    public boolean isStealth() {
        return stealth;
    }

    public void setStealth(boolean stealth) {
        this.stealth = stealth;
    }

    public Bitmap getPatternImage() {
        return patternImage;
    }

    public void setPatternImage(Bitmap patternImage) {
        this.patternImage = patternImage;
    }

    public String getPatternString() {
        return patternString;
    }

    public void setPatternString(String patternString) {
        this.patternString = patternString;
    }

}
