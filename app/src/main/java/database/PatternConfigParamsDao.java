package database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/** This class is used by the Room database engine to create the
 * necessary SQL statement for storing and retrieving the
 * Pattern Lock authentication mechanism's setups data from the app database.
 *
 * Author: Zauwali Sabitu Paki
 * April 2018
 */

@Dao
public interface PatternConfigParamsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConfigParams(PatternConfigParams configParams);

    @Query("SELECT * FROM PatternConfigParams ORDER BY configId ASC")
    LiveData<List<PatternConfigParams>> getAllConfigParams();

    @Query("SELECT * FROM PatternConfigParams ORDER BY configId ASC")
    List<PatternConfigParams> getAllSetupList();

    @Query("DELETE FROM PatternConfigParams")
    void deleteAllConfigParams();

    @Query("Delete from PatternConfigParams where configId = :configId")
    void deletePatternConfig(int configId);
}
