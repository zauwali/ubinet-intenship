package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/** Data Access Object class
 * Author: Zauwali S. Paki
 * April 2018
 */

@Dao
public interface PatternUserIdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLastUserId(PatternUserId data);

    @Query("SELECT * FROM PatternUserId Where configId = :configId")
    PatternUserId getLastUserId(int configId);

    @Query("DELETE FROM PatternUserId Where configId = :configId")
    void deleteLastUserId(int configId);

}
