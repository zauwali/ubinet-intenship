package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


/** This class is used to keep the Id of the user that performed the last
 * experiment. It is important in order to ensure that each time we run an
 * experiment, the user Id continues sequentially.
 *
 * Author: Zauwali S. Paki
 * Created on 29/08/2018
 */
@Entity
public class PincodUserId {
    @PrimaryKey
    private int configId;

    private int lastUserId;

    public PincodUserId(int configId, int lastUserId) {
        this.configId = configId;
        this.lastUserId = lastUserId;
    }

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public int getLastUserId() {
        return lastUserId;
    }

    public void setLastUserId(int lastUserId) {
        this.lastUserId = lastUserId;
    }
}
