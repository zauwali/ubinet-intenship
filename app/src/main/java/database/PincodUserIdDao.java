package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/** Data Access Object used by Room engine
 * Author: Zauwali S. Paki
 * April 2018
 * */

@Dao
public interface PincodUserIdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLastUserId(PincodUserId data);

    @Query("SELECT * FROM PincodUserId Where configId = :configId")
    PincodUserId getLastUserId(int configId);

    @Query("DELETE FROM PincodUserId Where configId = :configId")
    void deleteLastUserId(int configId);

}
