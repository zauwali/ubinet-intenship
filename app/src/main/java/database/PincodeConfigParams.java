package database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/** This class is used to provide database table for storing the
 * configuration parameters for the pincode authentication mechanism.
 * These configuration parameters will be used during experimentation
 * so user can save them and reuse them later
 *
 * Author: Zauwali S. Paki
 * April 2018
 */
@Entity(indices = {@Index(value = {"pincodeLength","maxTrials","shufflePad",
        "noIndicator", "showLastNum", "dotsToFill", "appearingDots"}, unique = true)})
public class PincodeConfigParams {

    @PrimaryKey(autoGenerate = true)
    int configId;
    private int pincodeLength;
    @NonNull
    String pinCode;
    private int maxTrials;
    private boolean shufflePad;
    private boolean noIndicator;
    private boolean showLastNum;
    private boolean dotsToFill;
    private boolean appearingDots;

    public PincodeConfigParams(int pincodeLength, @NonNull String pinCode, int maxTrials, boolean shufflePad, boolean noIndicator,
                               boolean showLastNum, boolean dotsToFill, boolean appearingDots) {
        this.pincodeLength = pincodeLength;
        this.pinCode = pinCode;
        this.maxTrials = maxTrials;
        this.shufflePad = shufflePad;
        this.noIndicator = noIndicator;
        this.showLastNum = showLastNum;
        this.dotsToFill = dotsToFill;
        this.appearingDots = appearingDots;
    }

    public int getPincodeLength() {
        return pincodeLength;
    }

    public String getPinCode(){return pinCode;}

    public void setPinCode(String pinCode){ this.pinCode = pinCode;}

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public void setPincodeLength(int pincodeLength) {
        this.pincodeLength = pincodeLength;
    }

    public int getMaxTrials() {
        return maxTrials;
    }

    public void setMaxTrials(int maxTrials) {
        this.maxTrials = maxTrials;
    }

    public boolean isShufflePad() {
        return shufflePad;
    }

    public void setShufflePad(boolean shufflePad) {
        this.shufflePad = shufflePad;
    }

    public boolean isNoIndicator() {
        return noIndicator;
    }

    public void setNoIndicator(boolean noIndicator) {
        this.noIndicator = noIndicator;
    }

    public boolean isShowLastNum() {
        return showLastNum;
    }

    public void setShowLastNum(boolean showLastNum) {
        this.showLastNum = showLastNum;
    }

    public boolean isDotsToFill() {
        return dotsToFill;
    }

    public void setDotsToFill(boolean dotsToFill) {
        this.dotsToFill = dotsToFill;
    }

    public boolean isAppearingDots() {
        return appearingDots;
    }

    public void setAppearingDots(boolean appearingDots) {
        this.appearingDots = appearingDots;
    }
}
