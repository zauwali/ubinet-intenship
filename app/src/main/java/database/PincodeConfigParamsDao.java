package database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/** This class is used by the Room database engine to create the
 * necessary SQL statement for storing and retrieving the
 * PIN code authentication mechanism's setups data from the app database.
 *
 * Author: Zauwali Sabitu Paki
 * April 2018
 */

@Dao
public interface PincodeConfigParamsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConfigParams(PincodeConfigParams configParams);

    @Query("SELECT * FROM PincodeConfigParams ORDER BY configId ASC")
    LiveData<List<PincodeConfigParams>> getAllConfigParams();

    @Query("SELECT * FROM PincodeConfigParams ORDER BY configId ASC")
    List<PincodeConfigParams> getAllSetupsList();


    @Query("DELETE FROM PincodeConfigParams")
    void deleteAllConfigParams();

    @Query("DELETE FROM PincodeConfigParams WHERE pincodeLength = :pincodeLength AND maxTrials = :numTrials " +
            "AND shufflePad = :shufflePad AND noIndicator = :noIndicator AND showLastNum = :showLastNum AND " +
            "dotsToFill = :dotsToFill AND appearingDots = :appearingDots")
    void deletePincodeConfig(int pincodeLength, int numTrials, boolean shufflePad, boolean noIndicator,
                             boolean showLastNum, boolean dotsToFill, boolean appearingDots);

    @Query("Delete from PincodeConfigParams Where configId = :configId")
    void deletePincodeConfig(int configId);

}
